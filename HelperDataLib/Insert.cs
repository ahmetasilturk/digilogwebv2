﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;
using System.Data;

namespace HelperDataLib
{
    public class Insert
    {

        public static void I_YETKI(string YETKIADI, string YETKIID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_YETKI(YT_YETKIID,YT_YETKIADI) values(@YT_YETKIID,@YT_YETKIADI)", con);
                com.Parameters.AddWithValue("@YT_YETKIID", YETKIID);
                com.Parameters.AddWithValue("@YT_YETKIADI", YETKIADI);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void I_YETKI_DETAY(string MODULID, bool OKU, bool YAZ, bool SIL, bool EKSTRA, string YETKIID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_YETKIDETAY(YTD_MODULID,YTD_OKU,YTD_YAZ,YTD_SIL,YTD_EKSTRA,YTD_YETKIID) values(@MODULID,@OKU,@YAZ,@SIL,@EKSTRA,@YETKIID)", con);
                com.Parameters.AddWithValue("@MODULID", MODULID);
                com.Parameters.AddWithValue("@OKU", OKU);
                com.Parameters.AddWithValue("@YAZ", YAZ);
                com.Parameters.AddWithValue("@SIL", SIL);
                com.Parameters.AddWithValue("@YETKIID", YETKIID);
                com.Parameters.AddWithValue("@EKSTRA", EKSTRA);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void I_KULLANICI(string USR_KULLANICIADI, string USR_ISIM, string USR_SOYISIM, string USR_SIFRE, int USR_YETKI, int USR_GRUP, int USR_DEPARTMAN, bool USR_AKTIF, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_KULLANICI(USR_KULLANICIADI,USR_ISIM,USR_SOYISIM,USR_SIFRE,USR_YETKI,USR_GRUP,USR_DEPARTMAN,USR_AKTIF) values(@USR_KULLANICIADI,@USR_ISIM,@USR_SOYISIM,@USR_SIFRE,@USR_YETKI,@USR_GRUP,@USR_DEPARTMAN,@USR_AKTIF)", con);
                com.Parameters.AddWithValue("@USR_KULLANICIADI", USR_KULLANICIADI);
                com.Parameters.AddWithValue("@USR_ISIM", USR_ISIM);
                com.Parameters.AddWithValue("@USR_SOYISIM", USR_SOYISIM);
                com.Parameters.AddWithValue("@USR_SIFRE", USR_SIFRE);
                com.Parameters.AddWithValue("@USR_YETKI", USR_YETKI);
                com.Parameters.AddWithValue("@USR_GRUP", USR_GRUP);
                com.Parameters.AddWithValue("@USR_DEPARTMAN", USR_DEPARTMAN);
                com.Parameters.AddWithValue("@USR_AKTIF", USR_AKTIF);

                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void I_GRUP(string GRP_KODU, string GRP_ADI, string GRP_USRKODU, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert I_GRUP(GRP_KODU,GRP_ADI,GRP_USRKODU) values(@GRP_KODU,@GRP_ADI,@GRP_USRKODU)", con);
                com.Parameters.AddWithValue("@GRP_KODU", GRP_KODU);
                com.Parameters.AddWithValue("@GRP_ADI", GRP_ADI);
                com.Parameters.AddWithValue("@GRP_USRKODU", GRP_USRKODU);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void I_SUBE(string BranchCode, string Name, bool IsActive, string Channel, bool IsConnected, DateTime ConnectedDate, string IpAddress, bool SqlExpress, string DataBase, string SSName, string City, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert Branch(BranchCode,Name,IsActive,Channel,IsConnected,ConnectedDate,IpAddress,SqlExpress,[DataBase],SSName,City) " +
                    "values(@BranchCode,@Name,@IsActive,@Channel,@IsConnected,@ConnectedDate,@IpAddress,@SqlExpress,@DataBase,@SSName,@City)", con);
                com.Parameters.AddWithValue("@BranchCode", BranchCode);
                com.Parameters.AddWithValue("@Name", Name);
                com.Parameters.AddWithValue("@IsActive", IsActive);
                com.Parameters.AddWithValue("@Channel", Channel);
                com.Parameters.AddWithValue("@IsConnected", IsConnected);
                com.Parameters.AddWithValue("@ConnectedDate", ConnectedDate);
                com.Parameters.AddWithValue("@IpAddress", IpAddress);
                com.Parameters.AddWithValue("@SqlExpress", SqlExpress);
                com.Parameters.AddWithValue("@DataBase", DataBase);
                com.Parameters.AddWithValue("@SSName", SSName);
                com.Parameters.AddWithValue("@City", City);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch(SqlException exp)
            {
                con.Close();
                durum = false;
                return;
            }
        }
       
        public static void I_DEPARTMAN(string Name, int BranchCode, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert Departments(Name,BranchCode) values(@Name,@BranchCode)", con);
                com.Parameters.AddWithValue("@Name", Name);
                com.Parameters.AddWithValue("@BranchCode", BranchCode);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void I_DAHILI(string Kanal, string Dahili, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert Channel(Channel_ChannelID,Channel_Number) values(@Channel_ChannelID,@Channel_Number)", con);
                com.Parameters.AddWithValue("@Channel_ChannelID", Kanal);
                com.Parameters.AddWithValue("@Channel_Number", Dahili);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void I_OZELGUN(string Holiday_Date, string Holiday_Memo, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("insert Holiday(Holiday_Date,Holiday_Memo) values(@Holiday_Date,@Holiday_Memo)", con);
                com.Parameters.AddWithValue("@Holiday_Date", Holiday_Date);
                com.Parameters.AddWithValue("@Holiday_Memo", Holiday_Memo);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                durum = false;
                return;
            }
        }

    }
}

