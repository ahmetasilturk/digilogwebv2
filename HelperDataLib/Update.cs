﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;

namespace HelperDataLib
{
    public class Update
    {
      
        public static void U_YETKI(string YETKIADI, string YETKIID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {
               
                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE I_YETKI SET YT_YETKIADI=@YT_YETKIADI WHERE YT_YETKIID=@YT_YETKIID", con);
                com.Parameters.AddWithValue("@YT_YETKIID", YETKIID);
                com.Parameters.AddWithValue("@YT_YETKIADI", YETKIADI);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void U_YETKI_DETAY(string MODULID, bool OKU, bool YAZ, bool SIL,bool EKSTRA,string YETKIID, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {
              
                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE I_YETKIDETAY SET YTD_OKU=@OKU,YTD_YAZ=@YAZ,YTD_SIL=@SIL,YTD_EKSTRA=@EKSTRA WHERE YTD_MODULID=@MODULID AND YTD_YETKIID=@YETKIID", con);
                com.Parameters.AddWithValue("@MODULID", MODULID);
                com.Parameters.AddWithValue("@OKU", OKU);
                com.Parameters.AddWithValue("@YAZ", YAZ);
                com.Parameters.AddWithValue("@SIL", SIL);
                com.Parameters.AddWithValue("@EKSTRA", EKSTRA);
                com.Parameters.AddWithValue("@YETKIID", YETKIID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }



        public static void U_KULLANICI(string USR_KULLANICIADI, string USR_ISIM, string USR_SOYISIM, string USR_SIFRE, int USR_YETKI, int USR_GRUP, int USR_DEPARTMAN, bool USR_AKTIF, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {
               
                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE I_KULLANICI SET USR_KULLANICIADI=@USR_KULLANICIADI,USR_ISIM=@USR_ISIM,USR_SOYISIM=@USR_SOYISIM,USR_SIFRE=@USR_SIFRE,USR_YETKI=@USR_YETKI,USR_GRUP=@USR_GRUP,USR_DEPARTMAN=@USR_DEPARTMAN,USR_AKTIF=@USR_AKTIF where USR_ID=@USR_ID", con);
                com.Parameters.AddWithValue("@USR_KULLANICIADI", USR_KULLANICIADI);
                com.Parameters.AddWithValue("@USR_ISIM", USR_ISIM);
                com.Parameters.AddWithValue("@USR_SOYISIM", USR_SOYISIM);
                com.Parameters.AddWithValue("@USR_SIFRE", USR_SIFRE);
                com.Parameters.AddWithValue("@USR_YETKI", USR_YETKI);
                com.Parameters.AddWithValue("@USR_GRUP", USR_GRUP);
                com.Parameters.AddWithValue("@USR_DEPARTMAN", USR_DEPARTMAN);
                com.Parameters.AddWithValue("@USR_AKTIF", USR_AKTIF);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void U_SIFRE(string USR_KULLANICIADI, string USR_SIFRE, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE I_KULLANICI SET USR_SIFRE=@USR_SIFRE where USR_KULLANICIADI=@USR_KULLANICIADI", con);
                com.Parameters.AddWithValue("@USR_KULLANICIADI", USR_KULLANICIADI);
                com.Parameters.AddWithValue("@USR_SIFRE", USR_SIFRE);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }


        public static void U_GRUP(string ID, string GRP_ADI, string GRP_USRKODU, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {
               
                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE I_GRUP SET GRP_ADI=@GRP_ADI,GRP_USRKODU=@GRP_USRKODU where GRP_ID=@ID", con);
                com.Parameters.AddWithValue("@ID", ID);
                com.Parameters.AddWithValue("@GRP_ADI", GRP_ADI);
                com.Parameters.AddWithValue("@GRP_USRKODU", GRP_USRKODU);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void U_DAHILI(int Channel_ID,string Kanal, string Dahili, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE Channel SET Channel_ChannelID=@Channel_ChannelID,Channel_Number=@Channel_Number where Channel_ID=@Channel_ID", con);
                com.Parameters.AddWithValue("@Channel_ChannelID", Kanal);
                com.Parameters.AddWithValue("@Channel_Number", Dahili);
                com.Parameters.AddWithValue("@Channel_ID", Channel_ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void U_OZELGUN(int Holiday_ID, string Holiday_Date, string Holiday_Memo, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE Holiday SET Holiday_Date=@Holiday_Date,Holiday_Memo=@Holiday_Memo where Holiday_ID=@Holiday_ID", con);
                com.Parameters.AddWithValue("@Holiday_Date", Holiday_Date);
                com.Parameters.AddWithValue("@Holiday_Memo", Holiday_Memo);
                com.Parameters.AddWithValue("@Holiday_ID", Holiday_ID);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void U_SUBE(string BranchCode, string Name, bool IsActive, string Channel, bool IsConnected, DateTime ConnectedDate, string IpAddress, bool SqlExpress, string DataBase, string SSName, string City, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("UPDATE Branch SET BranchCode=@BranchCode,Name=@Name,IsActive=@IsActive,Channel=@Channel,IsConnected=@IsConnected,ConnectedDate=@ConnectedDate,IpAddress=@IpAddress,SqlExpress=@SqlExpress,DataBase=@DataBase,SSName=@SSName,City=@City where BranchCode=@BranchCode", con);
                com.Parameters.AddWithValue("@BranchCode", BranchCode);
                com.Parameters.AddWithValue("@Name", Name);
                com.Parameters.AddWithValue("@IsActive", IsActive);
                com.Parameters.AddWithValue("@Channel", Channel);
                com.Parameters.AddWithValue("@IsConnected", IsConnected);
                com.Parameters.AddWithValue("@ConnectedDate", ConnectedDate);
                com.Parameters.AddWithValue("@IpAddress", IpAddress);
                com.Parameters.AddWithValue("@SqlExpress", SqlExpress);
                com.Parameters.AddWithValue("@DataBase", DataBase);
                com.Parameters.AddWithValue("@SSName", SSName);
                com.Parameters.AddWithValue("@City", City);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
    }
}
      
    

