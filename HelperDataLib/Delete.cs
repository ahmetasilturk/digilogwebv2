﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;

namespace HelperDataLib
{
   public class Delete
    {
     
       public static void D_GRUP(string Id, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {
             
               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE I_GRUP WHERE GRP_ID=@Id ", con);
               com.Parameters.AddWithValue("@Id", Id);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }
       public static void D_KULLANICI(string Id, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {
             
               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE FROM I_KULLANICI WHERE USR_ID=@Id ", con);
               com.Parameters.AddWithValue("@Id", Id);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }

       public static void D_YETKI(string ID, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           Connection.OpenConnection(ref con);
           try
           {
            
               SqlCommand com = new SqlCommand("DELETE FROM I_YETKI WHERE YT_ID=@ID", con);
               com.Parameters.AddWithValue("@ID", ID);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }
       public static void D_YETKI_DETAY(string ID, ref bool durum)
       {
           SqlConnection con = new SqlConnection();
           try
           {
               
               Connection.OpenConnection(ref con);
               SqlCommand com = new SqlCommand("DELETE FROM I_YETKIDETAY WHERE YTD_YETKIID=@ID", con);
               com.Parameters.AddWithValue("@ID", ID);
               int deger = com.ExecuteNonQuery();
               durum = true;
               con.Close();
           }
           catch
           {
               con.Close();
               durum = false;
               return;
           }
       }
        public static void D_SUBE(string Id, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM Branch WHERE BranchCode=@Id ", con);
                com.Parameters.AddWithValue("@Id", Id);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void D_DEPARTMAN(string Id, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM Departments WHERE ID=@Id ", con);
                com.Parameters.AddWithValue("@Id", Id);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

        public static void D_DAHILI(string Id, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM Channel WHERE Channel_ID=@Id ", con);
                com.Parameters.AddWithValue("@Id", Id);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }
        public static void D_OZELGUN(string Id, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("DELETE FROM Holiday WHERE Holiday_ID=@Id ", con);
                com.Parameters.AddWithValue("@Id", Id);
                int deger = com.ExecuteNonQuery();
                durum = true;
                con.Close();
            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }
        }

    }
}
