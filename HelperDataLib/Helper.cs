﻿using System;
using System.Collections.Generic;
using System.Data;

/// <summary>
/// Summary description for Class
/// </summary>
public enum ReportProcessType
{
    Play = 1,
    Download = 2,
    Email = 3
}

public enum BranchSqlSelection
{
    Branch,
    BackupBranch,
    Automatic
}

public class User
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public Role Role { get; set; }
    public int AuthorityBranch { get; set; }
    public Branch Branch { get; set; }
    public Branch SBranch { get; set; }
    public string Version { get; set; }

    public User()
    {
        Id = -1;
        UserName = "";
        Name = "";
        Password = "";
        AuthorityBranch = 0;
        Role = null;
        Branch = null;
        SBranch = null;
        Version = "2.3";
    }

    public User(DataRow dtrw)
    {
        try
        {
            Id = dtrw["UserId"] != DBNull.Value ? Convert.ToInt32(dtrw["UserId"]) : 0;
            UserName = dtrw["UserName"] != DBNull.Value ? dtrw["UserName"].ToString().Trim() : "";
            Name = dtrw["UName"] != DBNull.Value ? dtrw["UName"].ToString().Trim() : "";
            Password = dtrw["Password"] != DBNull.Value ? dtrw["Password"].ToString().Trim() : "";
            AuthorityBranch = 0;
            Role = null;
            Branch = null;
            SBranch = null;
            try
            {
                int roleId = dtrw["RoleId"] != DBNull.Value ? Convert.ToInt32(dtrw["RoleId"]) : 0;
                if (roleId > 0)
                    Role = new Role(dtrw);
            }
            catch { }

            try
            {
                int branchId = dtrw["BranchId"] != DBNull.Value ? Convert.ToInt32(dtrw["BranchId"]) : 0;
                if (branchId > 0)
                {
                    AuthorityBranch = branchId;
                    Branch = new Branch(dtrw);
                    SBranch = Branch;
                }
                else if (branchId < 1)
                    AuthorityBranch = -1;
                else if (Role != null && Role.Task1)
                    AuthorityBranch = -1;
            }
            catch { }
        }
        catch
        {
        }
    }
}

public class Branch
{
    public int Id { get; set; }
    public int BranchCode { get; set; }
    public string Name { get; set; }
    public bool IsActive { get; set; }
    public string Channel { get; set; }
    public DateTime ConnectedDate { get; set; }
    public bool IsConnected { get; set; }
    public string IpAddress { get; set; }
    public bool SqlExpress { get; set; }
    public string DataBase { get; set; }
    public string SSName { get; set; }
    public string RecordFileRoot { get; set; }
    public BranchSettings Settings { get; set; }

    public Branch()
    {
        Id = 0;
        BranchCode = 0;
        Name = "";
        Channel = "";
        IsConnected = false;
        ConnectedDate = DateTime.Now;
        IpAddress = "";
        SqlExpress = false;
        DataBase = "";
        SSName = "";
        RecordFileRoot = "";
        Settings = null;
    }

    public Branch(DataRow dtrw)
    {
        Id = dtrw["BranchId"] != DBNull.Value ? Convert.ToInt32(dtrw["BranchId"]) : 0;
        BranchCode = dtrw["BranchCode"] != DBNull.Value ? Convert.ToInt32(dtrw["BranchCode"]) : 0;
        Name = dtrw["BName"] != DBNull.Value ? dtrw["BName"].ToString().Trim() : "";
        IsActive = dtrw["IsActive"] != DBNull.Value ? Convert.ToBoolean(dtrw["IsActive"]) : true;
        Channel = dtrw["Channel"] != DBNull.Value ? dtrw["Channel"].ToString().Trim() : "";
        IsConnected = dtrw["IsConnected"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["IsConnected"].ToString()) ? Convert.ToBoolean(dtrw["IsConnected"]) : false;
        ConnectedDate = dtrw["ConnectedDate"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["ConnectedDate"].ToString()) ? Convert.ToDateTime(dtrw["ConnectedDate"]) : DateTime.Now;
        IpAddress = dtrw["IpAddress"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["IpAddress"].ToString()) ? dtrw["IpAddress"].ToString() : "";
        SqlExpress = dtrw["SqlExpress"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["SqlExpress"].ToString()) ? Convert.ToBoolean(dtrw["SqlExpress"]) : false;
        DataBase = dtrw["DataBase"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["DataBase"].ToString()) ? dtrw["DataBase"].ToString() : "";
        SSName = dtrw["SSName"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["SSName"].ToString()) ? dtrw["SSName"].ToString() : "";

        Settings = null;
    }
}

public class BranchSettings
{
    public int BranchCode { get; set; }
    public string IpAddress { get; set; }
    public string GlobalIpAddress { get; set; }
    public bool IsSearchBackup { get; set; }
    public string BackupIpAddress { get; set; }
    public string BackupSqlName { get; set; }
    public bool BackupSqlExpress { get; set; }
    public string BackupDataBase { get; set; }
    public string BackupTable { get; set; }
    public string BackupFileRoot { get; set; }
    public int SqlTimeout { get; set; }
    public string VirtualDirectory { get; set; }
    public string VirtualDirectory2 { get; set; }
    public string TotalDownloadRoot { get; set; }
    public string TDVirtualDirectory { get; set; }
    public string SMTPAddress { get; set; }
    public int SMTPPort { get; set; }
    public string MailUserName { get; set; }
    public string MailPassword { get; set; }
    public bool SSL { get; set; }
    public bool Credential { get; set; }
    public string MailFrom { get; set; }
    public string MailTo { get; set; }
    public System.Net.Mail.MailPriority MailPriority { get; set; }
    private int ParaType { get; set; }
    private int Id { get; set; }
    private string ParaName { get; set; }
    private string StrValue { get; set; }
    private int IntValue { get; set; }
    private float DblValue { get; set; }
    private bool BitValue { get; set; }
    private DateTime ModifiedDate { get; set; }
    private string RecordFileRoot { get; set; }
    public BranchSettings()
    {
        Id = 0;
        BranchCode = 0;
        ParaType = 0;
        ParaName = "";
        StrValue = "";
        IntValue = 0;
        DblValue = 0;
        GlobalIpAddress = "";
        IpAddress = "";
        SqlTimeout = 30;
        TDVirtualDirectory = "ConvertedFiles";
        BitValue = false;
        ModifiedDate = DateTime.Now;
        RecordFileRoot = "";
        MailPriority = System.Net.Mail.MailPriority.Normal;
    }

    public BranchSettings(DataTable dt)
    {
        try
        {
            SqlTimeout = 30;
            int timeout = 30;
            int sMTPPort = 0;
            bool vBool = false;
            bool sSL = false;
            bool credential = true;
            System.Net.Mail.MailPriority mailPriority = System.Net.Mail.MailPriority.Normal;

            TDVirtualDirectory = "ConvertedFiles";
            foreach (DataRow dtrw in dt.Rows)
            {
                BranchSettings settings = GetSettings(dtrw);
                BranchCode = settings.BranchCode;
                switch (settings.ParaName)
                {
                    case "VirtualDirectory":
                        VirtualDirectory = GetValue(settings).ToString();
                        break;
                    case "VirtualDirectory2":
                        VirtualDirectory2 = GetValue(settings).ToString();
                        break;
                    case "TotalDownloadRoot":
                        TotalDownloadRoot = GetValue(settings).ToString();
                        break;
                    case "TDVirtualDirectory":
                        TDVirtualDirectory = GetValue(settings).ToString();
                        break;
                    case "SMTPAddress":
                        SMTPAddress = GetValue(settings).ToString();
                        break;
                    case "MailUserName":
                        MailUserName = GetValue(settings).ToString();
                        break;
                    case "MailPassword":
                        MailPassword = GetValue(settings).ToString();
                        break;
                    case "SMTPPort":
                        int.TryParse(GetValue(settings).ToString(), out sMTPPort);
                        SMTPPort = sMTPPort;
                        break;
                    case "SSL":
                        bool.TryParse(GetValue(settings).ToString(), out sSL);
                        SSL = sSL;
                        break;
                    case "Credential":
                        bool.TryParse(GetValue(settings).ToString(), out credential);
                        Credential = credential;
                        break;
                    case "MailFrom":
                        MailFrom = GetValue(settings).ToString();
                        break;
                    case "MailTo":
                        MailTo = GetValue(settings).ToString();
                        break;
                    case "MailPriority":
                        Enum.TryParse(GetValue(settings).ToString(), out mailPriority);
                        MailPriority = mailPriority;
                        break;
                    case "IpAddress":
                        IpAddress = GetValue(settings).ToString();
                        break;
                    case "GlobalIpAddress":
                        GlobalIpAddress = GetValue(settings).ToString();
                        break;
                    case "SqlTimeout":
                        if (!int.TryParse(GetValue(settings).ToString(), out timeout))
                            timeout = 30;
                        SqlTimeout = timeout;
                        break;
                    case "IsSearchBackup":
                        bool.TryParse(GetValue(settings).ToString(), out vBool);
                        IsSearchBackup = vBool;
                        break;
                    case "BackupIpAddress":
                        BackupIpAddress = GetValue(settings).ToString();
                        break;
                    case "BackupSqlName":
                        BackupSqlName = GetValue(settings).ToString();
                        break;
                    case "BackupSqlExpress":
                        bool.TryParse(GetValue(settings).ToString(), out vBool);
                        BackupSqlExpress = vBool;
                        break;
                    case "BackupDataBase":
                        BackupDataBase = GetValue(settings).ToString();
                        break;
                    case "BackupTable":
                        BackupTable = GetValue(settings).ToString();
                        break;
                    case "BackupFileRoot":
                        BackupFileRoot = GetValue(settings).ToString();
                        break;
                    case "RecordFileRoot":
                        RecordFileRoot = GetValue(settings).ToString();
                        break;
                    default:
                        break;
                }
            }
        }
        catch { }
    }

    public BranchSettings GetSettings(DataRow dtrw)
    {
        BranchSettings settings = null;
        try
        {
            settings = new BranchSettings();
            settings.Id = dtrw["Id"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["Id"].ToString()) ? Convert.ToInt32(dtrw["Id"]) : 0;
            settings.BranchCode = dtrw["BranchCode"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["BranchCode"].ToString()) ? Convert.ToInt32(dtrw["BranchCode"]) : 0;
            settings.ParaType = dtrw["ParaType"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["ParaType"].ToString()) ? Convert.ToInt32(dtrw["ParaType"]) : 0;
            settings.ParaName = dtrw["ParaName"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["ParaName"].ToString()) ? dtrw["ParaName"].ToString().Trim() : "";
            settings.StrValue = dtrw["StrValue"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["StrValue"].ToString()) ? dtrw["StrValue"].ToString().Trim() : "";
            settings.IntValue = dtrw["IntValue"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["IntValue"].ToString()) ? Convert.ToInt32(dtrw["IntValue"]) : 0;
            settings.DblValue = dtrw["DblValue"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["DblValue"].ToString()) ? Convert.ToInt64(dtrw["DblValue"]) : 0;
            settings.BitValue = dtrw["BitValue"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["BitValue"].ToString()) ? Convert.ToBoolean(dtrw["BitValue"]) : false;
            settings.ModifiedDate = dtrw["ModifiedDate"] != DBNull.Value && !string.IsNullOrWhiteSpace(dtrw["ModifiedDate"].ToString()) ? Convert.ToDateTime(dtrw["ModifiedDate"]) : DateTime.Now;
        }
        catch { settings = null; }
        return settings;
    }

    private object GetValue(BranchSettings settings)
    {
        object val = null;
        switch (settings.ParaType)
        {
            case 0:
                val = settings.StrValue;
                break;
            case 1:
                val = settings.IntValue;
                break;
            case 2:
                val = settings.DblValue;
                break;
            case 3:
                val = settings.BitValue;
                break;
            default:
                break;
        }
        return val;
    }
}

public class Role
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool Task1 { get; set; }
    public int Tsk1 { get { return Task1 == true ? 101 : 0; } }
    public bool Task2 { get; set; }
    public int Tsk2 { get { return Task2 == true ? 102 : 0; } }
    public bool Task3 { get; set; }
    public int Tsk3 { get { return Task3 == true ? 103 : 0; } }
    public bool Task4 { get; set; }
    public int Tsk4 { get { return Task4 == true ? 104 : 0; } }
    public bool Task5 { get; set; }
    public int Tsk5 { get { return Task5 == true ? 105 : 0; } }
    public bool Task6 { get; set; }
    public int Tsk6 { get { return Task6 == true ? 106 : 0; } }
    public bool Task7 { get; set; }
    public int Tsk7 { get { return Task7 == true ? 107 : 0; } }
    public bool Task8 { get; set; }
    public int Tsk8 { get { return Task8 == true ? 108 : 0; } }
    public bool Task9 { get; set; }
    public int Tsk9 { get { return Task9 == true ? 109 : 0; } }
    public bool Task10 { get; set; }
    public int Tsk10 { get { return Task10 == true ? 110 : 0; } }
    public bool Task11 { get; set; }
    public int Tsk11 { get { return Task11 == true ? 111 : 0; } }
    public bool Task12 { get; set; }
    public int Tsk12 { get { return Task12 == true ? 112 : 0; } }
    public bool Task13 { get; set; }
    public int Tsk13 { get { return Task13 == true ? 113 : 0; } }
    public bool Task14 { get; set; }
    public int Tsk14 { get { return Task14 == true ? 114 : 0; } }
    public bool Task15 { get; set; }
    public int Tsk15 { get { return Task15 == true ? 115 : 0; } }

    public Role()
    {
        Id = 0;
        Name = "";
        Task1 = false;
        Task2 = false;
        Task3 = false;
        Task4 = false;
        Task5 = false;
        Task6 = false;
        Task7 = false;
        Task8 = false;
        Task9 = false;
        Task10 = false;
        Task11 = false;
        Task12 = false;
        Task13 = false;
        Task14 = false;
        Task15 = false;
    }

    public Role(DataRow dtrw)
    {
        Id = dtrw["RoleId"] != DBNull.Value ? Convert.ToInt32(dtrw["RoleId"]) : 0;
        Name = dtrw["RName"] != DBNull.Value ? dtrw["RName"].ToString().Trim() : "";
        Task1 = dtrw["Task1"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task1"]) : false;
        Task2 = dtrw["Task2"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task2"]) : false;
        Task3 = dtrw["Task3"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task3"]) : false;
        Task4 = dtrw["Task4"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task4"]) : false;
        Task5 = dtrw["Task5"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task5"]) : false;
        Task6 = dtrw["Task6"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task6"]) : false;
        Task7 = dtrw["Task7"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task7"]) : false;
        Task8 = dtrw["Task8"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task8"]) : false;
        Task9 = dtrw["Task9"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task9"]) : false;
        Task10 = dtrw["Task10"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task10"]) : false;
        Task11 = dtrw["Task11"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task11"]) : false;
        Task12 = dtrw["Task12"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task12"]) : false;
        Task13 = dtrw["Task13"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task13"]) : false;
        Task14 = dtrw["Task14"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task14"]) : false;
        Task15 = dtrw["Task15"] != DBNull.Value ? Convert.ToBoolean(dtrw["Task15"]) : false;
    }
}

public class StorageDisc
{
    public string Name { get; set; }
    public object Size { get; set; }
    public object FreeSpace { get; set; }
    public decimal Usage { get; set; }
    public string Sz { get; set; }
    public string FrSpc { get; set; }
    public string Usg { get; set; }
    public string Free { get; set; }
    public int Wdh { get; set; }
    public int Wdth { get; set; }

    public StorageDisc()
    {

    }
}

public class Region
{
    public int RId { get; set; }
    public string Text { get; set; }
    public string Url { get; set; }
    public string Ip { get; set; }
    public string IpAddress { get; set; }
    public string SubDomain { get; set; }
    public string ApplicationStatus { get; set; }
    public string DiskResult { get; set; }
    public string Canal { get; set; }
    public System.Drawing.Color Color { get; set; }
    public Boolean IsWarning { get; set; }
    public string IsWarn { get; set; }
    public List<Warning> WarningMessages { get; set; }
    public string WIds { get; set; }
    public string FwIds { get; set; }
    public string WMessage { get; set; }
    public int BranchID { get; set; }
    public int BranchCode { get; set; }
    public string ImageUrl { get; set; }
    public string Files { get; set; }
    public string IsFile { get; set; }
    public string FImageUrl { get; set; }

    public Region()
    { }
}

public class Warning
{
    public int id { get; set; }
    public int nid { get; set; }
    public string sender { get; set; }
    public string title { get; set; }
    public string details { get; set; }
    public int state { get; set; }
    public DateTime createtime { get; set; }
    public DateTime lastactivetime { get; set; }
    public Boolean isRead { get; set; }

    public Warning()
    { }
    public Warning(DataRow dtrw)
    {
        nid = dtrw["nid"] != DBNull.Value ? Convert.ToInt32(dtrw["nid"]) : -1;
        sender = dtrw["sender"] != DBNull.Value ? dtrw["sender"].ToString() : String.Empty;
        title = dtrw["title"] != DBNull.Value ? dtrw["title"].ToString() : String.Empty;
        details = dtrw["details"] != DBNull.Value ? dtrw["details"].ToString() : String.Empty;
        state = dtrw["state"] != DBNull.Value ? Convert.ToInt32(dtrw["state"]) : -1;
        createtime = dtrw["createtime"] != DBNull.Value ? Convert.ToDateTime(dtrw["createtime"]) : DateTime.MinValue;
        lastactivetime = dtrw["lastactivetime"] != DBNull.Value ? Convert.ToDateTime(dtrw["lastactivetime"]) : DateTime.MinValue;
    }
}

public class LocalValue
{
    public int Type { get; set; }
    public string strValue { get; set; }
    public decimal intValue { get; set; }
    public decimal intValue2 { get; set; }
    public decimal intValue3 { get; set; }
    public string description { get; set; }


    public LocalValue()
    { }

    public LocalValue(DataRow dtrw)
    {
        Type = dtrw["Type"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["Type"].ToString()) ? Convert.ToInt32(dtrw["Type"]) : -1;
        strValue = dtrw["strValue"] != DBNull.Value ? dtrw["strValue"].ToString() : String.Empty;
        intValue = dtrw["intValue"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["intValue"].ToString()) ? Convert.ToDecimal(dtrw["intValue"]) : 0;
        intValue2 = dtrw["intValue2"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["intValue2"].ToString()) ? Convert.ToDecimal(dtrw["intValue2"]) : 0;
        intValue3 = dtrw["intValue3"] != DBNull.Value && !string.IsNullOrEmpty(dtrw["intValue3"].ToString()) ? Convert.ToDecimal(dtrw["intValue3"]) : 0;
        description = dtrw["description"] != DBNull.Value ? dtrw["description"].ToString() : String.Empty;
    }
}

public class Person
{
    public int rid { get; set; }
    public string Name { get; set; }
    public string Ext { get; set; }
    public int Chn_Id { get; set; }
    public string Ext_No { get; set; }
    public int State { get; set; }

    public Person()
    {
        Chn_Id = 0;
        Ext_No = string.Empty;
        State = 0;
    }

    public Person(DataRow dtrw)
    {
        try
        {
            Chn_Id = dtrw["chn_id"] != DBNull.Value && dtrw["chn_id"].ToString() != String.Empty ? Convert.ToInt32(dtrw["chn_id"]) : -1;
            if (Chn_Id > -1)
            {
                rid = dtrw["rid"] != DBNull.Value && dtrw["rid"].ToString() != String.Empty ? Convert.ToInt32(dtrw["rid"]) : -1;
                Ext_No = dtrw["extno"] != DBNull.Value && !String.IsNullOrEmpty(dtrw["extno"].ToString()) ? dtrw["extno"].ToString() : "";
                Name = dtrw["chn_agentname"] != DBNull.Value && !String.IsNullOrEmpty(dtrw["chn_agentname"].ToString()) ? dtrw["chn_agentname"].ToString() : "";
                State = dtrw["state"] != DBNull.Value && !String.IsNullOrEmpty(dtrw["state"].ToString()) ? Convert.ToInt32(dtrw["state"]) : -1;
            }
        }
        catch { }
    }
}

public class Department
{
    public int ID { get; set; }
    public string Name { get; set; }
    public string BranchName { get; set; }
    public int BranchCode { get; set; }
    public List<int> Extensions { get; set; }
    public int UserId { get; set; }
    public string Exts { get; set; }
    public int BranchId { get; set; }
    public Department()
    {
        BranchId = -1;
        ID = -1;
        Name = string.Empty;
        BranchCode = 0;
        Extensions = new List<int>();
        Exts = "0";
        BranchName = string.Empty;
    }

    public Department(DataRow dtrw)
    {
        ID = dtrw["ID"] != DBNull.Value && dtrw["ID"].ToString() != String.Empty ? Convert.ToInt32(dtrw["ID"]) : -1;
        Name = dtrw["Name"] != DBNull.Value && !String.IsNullOrEmpty(dtrw["Name"].ToString()) ? dtrw["Name"].ToString() : "";
        BranchCode = dtrw["BranchCode"] != DBNull.Value && !String.IsNullOrWhiteSpace(dtrw["BranchCode"].ToString()) ? Convert.ToInt32(dtrw["BranchCode"]) : 0;
        BranchName = dtrw["BranchName"] != DBNull.Value && !String.IsNullOrWhiteSpace(dtrw["BranchName"].ToString()) ? dtrw["BranchName"].ToString() : "";
        BranchId = dtrw["BranchId"] != DBNull.Value && !String.IsNullOrWhiteSpace(dtrw["BranchId"].ToString()) ? Convert.ToInt32(dtrw["BranchId"]) : 0;
        Extensions = new List<int>();
    }
}

public class Extensions
{
    public int Ext { get; set; }

    public Extensions()
    { Ext = 0; }
}

public class Summary
{
    public int ITCount { get; set; }
    public string ITDur { get; set; }
    public int OTCount { get; set; }
    public string OTDur { get; set; }
    public int UTCount { get; set; }
    public string UTDur { get; set; }
    public string TDur { get; set; }

    public Summary()
    {
        ITCount = 0;
        ITDur = "00:00:00";
        OTCount = 0;
        OTDur = "00:00:00";
        UTCount = 0;
        UTDur = "00:00:00";
        TDur = "00:00:00";
    }
}

public class ChnnlDetail
{
    public int rid { get; set; }
    public int chnid { get; set; }
    public string extno { get; set; }
    public string chn_agentcode { get; set; }
    public string agentcode { get; set; }
    public string chn_agentname { get; set; }
    public string psw { get; set; }
    public int function { get; set; }
    public Int64 trigger { get; set; }
    public Int64 sil_time { get; set; }
    public Int64 option { get; set; }
    public Int64 lowthreshold { get; set; }
    public Int64 highthreshold { get; set; }
    public string dtmfstart { get; set; }
    public string dtmfstop { get; set; }
    public int rcvcidtype { get; set; }
    public int offhookevent { get; set; }
    public int onhookevent { get; set; }
    public int state { get; set; }
    public int rectime { get; set; }

    public ChnnlDetail()
    { }

    public ChnnlDetail(DataRow dtrw)
    {
        rid = !string.IsNullOrEmpty(dtrw["rid"].ToString()) && dtrw["rid"] != DBNull.Value ? Convert.ToInt32(dtrw["rid"]) : -1;
        extno = !string.IsNullOrEmpty(dtrw["extno"].ToString()) && dtrw["extno"] != DBNull.Value ? dtrw["extno"].ToString() : "";
        chn_agentcode = !string.IsNullOrEmpty(dtrw["chn_agentcode"].ToString()) && dtrw["chn_agentcode"] != DBNull.Value ? dtrw["chn_agentcode"].ToString() : "";
        chn_agentname = !string.IsNullOrEmpty(dtrw["chn_agentname"].ToString()) && dtrw["chn_agentname"] != DBNull.Value ? dtrw["chn_agentname"].ToString() : "";
        psw = !string.IsNullOrEmpty(dtrw["psw"].ToString()) && dtrw["psw"] != DBNull.Value ? dtrw["psw"].ToString() : "";
        function = !string.IsNullOrEmpty(dtrw["function"].ToString()) && dtrw["function"] != DBNull.Value ? Convert.ToInt32(dtrw["function"]) : -1;
        trigger = !string.IsNullOrEmpty(dtrw["trigger"].ToString()) && dtrw["trigger"] != DBNull.Value ? Convert.ToInt32(dtrw["trigger"]) : -1;
        sil_time = !string.IsNullOrEmpty(dtrw["sil_time"].ToString()) && dtrw["sil_time"] != DBNull.Value ? Convert.ToInt32(dtrw["sil_time"]) : -1;
        option = !string.IsNullOrEmpty(dtrw["option"].ToString()) && dtrw["option"] != DBNull.Value ? Convert.ToInt32(dtrw["option"]) : -1;
        rectime = !string.IsNullOrEmpty(dtrw["rectime"].ToString()) && dtrw["rectime"] != DBNull.Value ? Convert.ToInt32(dtrw["rectime"]) : -1;
        lowthreshold = !string.IsNullOrEmpty(dtrw["lowthreshold"].ToString()) && dtrw["lowthreshold"] != DBNull.Value ? Convert.ToInt32(dtrw["lowthreshold"]) : -1;
        highthreshold = !string.IsNullOrEmpty(dtrw["highthreshold"].ToString()) && dtrw["highthreshold"] != DBNull.Value ? Convert.ToInt32(dtrw["highthreshold"]) : -1;
        dtmfstart = !string.IsNullOrEmpty(dtrw["dtmfstart"].ToString()) && dtrw["dtmfstart"] != DBNull.Value ? dtrw["dtmfstart"].ToString() : "";
        dtmfstop = !string.IsNullOrEmpty(dtrw["dtmfstop"].ToString()) && dtrw["dtmfstop"] != DBNull.Value ? dtrw["dtmfstop"].ToString() : "";
        rcvcidtype = !string.IsNullOrEmpty(dtrw["rcvcidtype"].ToString()) && dtrw["rcvcidtype"] != DBNull.Value ? Convert.ToInt32(dtrw["rcvcidtype"]) : -1;
        offhookevent = !string.IsNullOrEmpty(dtrw["offhookevent"].ToString()) && dtrw["offhookevent"] != DBNull.Value ? Convert.ToInt32(dtrw["offhookevent"]) : -1;
        onhookevent = !string.IsNullOrEmpty(dtrw["onhookevent"].ToString()) && dtrw["onhookevent"] != DBNull.Value ? Convert.ToInt32(dtrw["onhookevent"]) : -1;
        state = !string.IsNullOrEmpty(dtrw["state"].ToString()) && dtrw["state"] != DBNull.Value ? Convert.ToInt32(dtrw["state"]) : -1;
    }
}

public class RecordChart
{
    public DateTime datetime { get; set; }
    public DateTime date { get; set; }
    public TimeSpan time { get; set; }
    public int Type { get; set; }
    public TimeSpan TotalTime { get; set; }
    public string Extension { get; set; }
}



public class TotalDownload
{
    public int Ref { get; set; }
    public int RecordRef { get; set; }
    public bool IsConvert { get; set; }
    public string SourceFile { get; set; }
    public string Name { get; set; }
    public string DestFile { get; set; }
    public string WebDestFile { get; set; }
    public string Ext { get; set; }
    public bool IsOk { get; set; }
    public string Caller { get; set; }
    public string Called { get; set; }
    public string Image
    {
        get
        {
            string rVal = "";
            if (IsOk)
                rVal = "images/ok.png";
            else
                rVal = "images/warning.png";
            return rVal;
        }
    }
}

public class OpenChildForm
{
    public string FormAddress { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public string Name { get; set; }
    public Boolean Scrollbars { get; set; }
    public Boolean Resizable { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
}
