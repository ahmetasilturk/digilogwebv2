﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Web;
using System.Configuration;
using System.IO;
using System.Data.OleDb;
using System.Data;
namespace HelperDataLib
{
    public class Select
    {

        public static SqlConnection con = new SqlConnection();

        public static void S_PAGE_YETKIKOTROL(string MODULID)
        {
            bool durum = false;
            string[] arg = new string[999999];
            arg = HttpContext.Current.Session["SS_MODULID"].ToString().Split(',');
            foreach (string item in arg)
            {

                if (item == MODULID)
                {
                    durum = true;
                }

            }
            if (durum == false)
            {
                HttpContext.Current.Response.Redirect("~/Default.aspx");
            }
        }
        public static void S_KULLANICIGIRIS(string Username, string Sifre, bool check, ref bool durum)
        {
            try
            {

                string ip = HttpContext.Current.Request.UserHostAddress.ToString();
                string browser = HttpContext.Current.Request.Browser.Browser.ToString() + " " + HttpContext.Current.Request.Browser.Version.ToString();
                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT [USR_ID],[USR_KULLANICIADI],[USR_ISIM],[USR_SOYISIM],[USR_SIFRE],[USR_YETKI],[USR_GRUP],[USR_DEPARTMAN],[USR_AKTIF],[GRP_ADI],[GRP_USRKODU],[YT_YETKIID],[YT_YETKIADI] FROM [I_KULLANICI] USR " +
                    "JOIN I_YETKI Y  ON  USR.USR_YETKI=Y.YT_YETKIID JOIN I_GRUP G ON USR.USR_GRUP=G.GRP_KODU   WHERE USR_KULLANICIADI=@UserName AND USR_SIFRE=@Password AND USR_AKTIF='True'", con);
                com.Parameters.AddWithValue("@UserName", Username);
                com.Parameters.AddWithValue("@Password", Sifre);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {
                    durum = true;
                    if (check == true)
                    {
                        HttpCookie oKCookie = new HttpCookie("girismail");
                        oKCookie.Value = Username.Trim();
                        oKCookie.Expires = DateTime.Now.AddMonths(1);
                        HttpContext.Current.Response.Cookies.Add(oKCookie);
                        HttpCookie oKCookieS = new HttpCookie("girissifre");
                        oKCookieS.Value = Sifre.Trim();
                        oKCookieS.Expires = DateTime.Now.AddMonths(1);
                        HttpContext.Current.Response.Cookies.Add(oKCookieS);
                    }
                    HttpContext.Current.Session["SS_USERNAME"] = SDR["USR_KULLANICIADI"].ToString();
                    //HttpContext.Current.Session["SS_MAIL"] = SDR["EMail"].ToString();
                    HttpContext.Current.Session["SS_ID"] = SDR["USR_ID"].ToString();
                    HttpContext.Current.Session["SS_ISIMSOYISIM"] = SDR["USR_ISIM"].ToString() + " " + SDR["USR_SOYISIM"].ToString();
                    //HttpContext.Current.Session["SS_TELEFON"] = SDR["Telefon"].ToString();
                    HttpContext.Current.Session["SS_GRUPUSERS"] = SDR["GRP_USRKODU"].ToString();
                    HttpContext.Current.Session["SS_GRUPADI"] = SDR["GRP_ADI"].ToString();
                    HttpContext.Current.Session["SS_YETKIID"] = SDR["YT_YETKIID"].ToString();
                    HttpContext.Current.Session["SS_YETKIADI"] = SDR["YT_YETKIADI"].ToString();
                    S_MUSTERIMODUL_GET();
                    HttpContext.Current.Response.Redirect("Default.aspx", false);
                }
                else
                {
                    durum = false;
                }
                con.Close();

            }
            catch
            {
                con.Close();
                durum = false;
                return;
            }

        }

        public static void S_MUSTERIMODUL_GET()
        {
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT MUS_MODULID,MUS_SUBMODULID FROM I_MUSTERIMODUL", con);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {
                    HttpContext.Current.Session["SS_MODULID"] = SDR["MUS_MODULID"].ToString();
                    HttpContext.Current.Session["SS_SUBMODULID"] = SDR["MUS_SUBMODULID"].ToString();

                }
                con.Close();
            }
            catch
            {
                con.Close();
                return;
            }

        }

        public static System.Data.DataTable S_MODUL_YUKLE()
        {
            string id = HttpContext.Current.Session["SS_SUBMODULID"].ToString();
            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM I_MODUL WHERE MDL_MODULID IN (" + id + ")";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.SelectCommand.Parameters.AddWithValue("@MODULID", id);

            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static void S_YETKI_GETVALUES(string YETKIID, ref bool OKU, ref bool YAZ, ref bool SIL, ref bool EKSTRA, ref string USERS)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM I_YETKIDETAY WHERE  YTD_YETKIID=@YETKIID", con);
                // com.Parameters.AddWithValue("@YETKIID", HttpContext.Current.Session["SS_YETKIID"].ToString());
                // com.Parameters.AddWithValue("@MODULID", MODULID);
                com.Parameters.AddWithValue("@YETKIID", YETKIID);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    OKU = Convert.ToBoolean(SDR["YTD_OKU"].ToString());
                    YAZ = Convert.ToBoolean(SDR["YTD_YAZ"].ToString());
                    SIL = Convert.ToBoolean(SDR["YTD_SIL"].ToString());
                    EKSTRA = Convert.ToBoolean(SDR["YTD_EKSTRA"].ToString());
                    USERS = S_GETGRUPUSERS();
                }

                con.Close();
            }
            catch
            {
                con.Close();
                return;
            }

        }
        public static void S_YETKI_GETVALUES_DETAY(string YETKIID, string MODULID, ref bool OKU, ref bool YAZ, ref bool SIL, ref bool EKSTRA, ref string USERS)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM I_YETKIDETAY WHERE  YTD_YETKIID=@YETKIID AND YTD_MODULID=@MODULID", con);
                com.Parameters.AddWithValue("@YETKIID", YETKIID);
                com.Parameters.AddWithValue("@MODULID", MODULID);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    OKU = Convert.ToBoolean(SDR["YTD_OKU"].ToString());
                    YAZ = Convert.ToBoolean(SDR["YTD_YAZ"].ToString());
                    SIL = Convert.ToBoolean(SDR["YTD_SIL"].ToString());
                    EKSTRA = Convert.ToBoolean(SDR["YTD_EKSTRA"].ToString());
                    USERS = S_GETGRUPUSERS();
                }

                con.Close();
            }
            catch
            {
                con.Close();
                return;
            }

        }
        public static string S_GETGRUPUSERS()
        {
            SqlConnection con = new SqlConnection(); string USR_KULLANCIKODU = "";
            try
            {


                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT USR_KULLANCIKODU FROM I_USERS WHERE USR_GRUPKODU=@ID", con);
                com.Parameters.AddWithValue("@ID", HttpContext.Current.Session["SS_GRUP"].ToString());
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {
                    USR_KULLANCIKODU = SDR["USR_KULLANCIKODU"].ToString();
                }
                con.Close();
            }
            catch
            {
                con.Close();
            }
            return USR_KULLANCIKODU;
        }
        public static System.Data.DataTable S_USER_LISTESI()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT [USR_ID],[USR_KULLANICIADI],[USR_ISIM],[USR_SOYISIM],[USR_SIFRE],[USR_YETKI],[USR_GRUP],[USR_DEPARTMAN],[USR_AKTIF],[GRP_ADI],[GRP_USRKODU],[YT_YETKIID],[YT_YETKIADI] FROM [I_KULLANICI] USR " +
                    "JOIN I_YETKI Y  ON  USR.USR_YETKI=Y.YT_YETKIID JOIN I_GRUP G ON USR.USR_GRUP=G.GRP_KODU ";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static System.Data.DataTable S_YETKI_YUKLE()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM I_YETKI";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }
        public static System.Data.DataTable S_YETKI_MODUL_LISTESI()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT MDL_MODULID FROM I_MODUL";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static System.Data.DataTable S_GRUP_LISTESI()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM I_GRUP";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static System.Data.DataTable S_SUBE_YUKLE()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM Branch";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static System.Data.DataTable S_DEPARTMAN_YUKLE()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM Departments";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static System.Data.DataTable S_DAHILI_YUKLE()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM Channel";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }


        public static System.Data.DataTable S_LOG_YUKLE(DateTime baslama, DateTime bitis, string userid)
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT USR_KULLANICIADI,Description ,ProcessDate ,'{0}' AS Islem FROM Logs INNER JOIN I_KULLANICI ON I_KULLANICI.USR_ID = Logs.UserId WHERE ProcessDate>=@TARIH1 AND ProcessDate<=@TARIH2";
            if (userid != "")
            {
                sorgu = sorgu + " AND USR_ID=@USR_ID";
            }
            //if (code != "0")
            //{
            //    sorgu = sorgu + " AND Code=@Code";
            //}
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.SelectCommand.Parameters.AddWithValue("@TARIH1", baslama);
            daa.SelectCommand.Parameters.AddWithValue("@TARIH2", bitis);
            daa.SelectCommand.Parameters.AddWithValue("@USR_ID", userid);
            //daa.SelectCommand.Parameters.AddWithValue("@Code", code);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }
        public static System.Data.DataTable S_OZELGUN_YUKLE()
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = "SELECT * FROM Holiday";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }

        public static string S_GETYETKIADI_KONTROL(string YETKIADI)
        {
            string yetkiadi = "";
            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            SqlCommand com = new SqlCommand("SELECT YT_YETKIADI FROM I_YETKI where YT_YETKIADI=@YETKIADI", con);
            com.Parameters.AddWithValue("@YETKIADI", YETKIADI);
            SqlDataReader SDR = com.ExecuteReader();
            if (SDR.Read())
            {
                yetkiadi = SDR["YT_YETKIADI"].ToString();
            }
            con.Close();
            return yetkiadi;
        }
        public static int S_GETMAXYETKIID()
        {
            int sayi = 0;
            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            SqlCommand com = new SqlCommand("SELECT ISNULL(MAX(YT_ID),0)+1 AS SAYI FROM I_YETKI", con);
            SqlDataReader SDR = com.ExecuteReader();
            if (SDR.Read())
            {
                sayi = Convert.ToInt32(SDR["SAYI"].ToString());
            }
            con.Close();
            return sayi;
        }
        public static int S_GETKULLANICIID()
        {
            int sayi = 0;
            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            SqlCommand com = new SqlCommand("SELECT ISNULL(MAX(USR_ID),0)+1 AS SAYI FROM I_KULLANICI", con);
            SqlDataReader SDR = com.ExecuteReader();
            if (SDR.Read())
            {
                sayi = Convert.ToInt32(SDR["SAYI"].ToString());
            }
            con.Close();
            return sayi;
        }
        public static int S_GETMAXGRUP()
        {
            int sayi = 0;
            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            SqlCommand com = new SqlCommand("SELECT ISNULL(MAX(GRP_ID),0)+1 AS SAYI FROM I_GRUP", con);
            SqlDataReader SDR = com.ExecuteReader();
            if (SDR.Read())
            {
                sayi = Convert.ToInt32(SDR["SAYI"].ToString());
            }
            con.Close();
            return sayi;
        }
        public static void S_USERS_GETDETAY(string Id, ref string USR_KULLANCIKODU, ref string USR_ADI, ref string USR_SIFRE, ref string USR_YETKIKODU, ref string USR_YETKIADI, ref string USR_GRUPKODU, ref string USR_GRUPADI, ref string USR_DEPARTMAN, ref bool USR_DURUM)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM I_KULLANICI U Left Join I_YETKI Y on U.USR_YETKI=Y.YT_YETKIID left join I_GRUP G ON G.GRP_KODU=U.USR_GRUP LEFT JOIN Departments D on U.USR_DEPARTMAN = D.ID WHERE U.USR_ID=@Id", con);
                com.Parameters.AddWithValue("@Id", Id);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    USR_KULLANCIKODU = SDR["USR_KULLANICIADI"].ToString();
                    USR_ADI = SDR["USR_ISIM"].ToString() + " " + SDR["USR_SOYISIM"].ToString();
                    // USR_EXTEN = SDR["Extension"].ToString();
                    // USR_MAIL = SDR["EMail"].ToString();
                    USR_SIFRE = SDR["USR_SIFRE"].ToString();
                    USR_DEPARTMAN = SDR["Name"].ToString();
                    USR_YETKIKODU = SDR["YT_YETKIID"].ToString();
                    USR_YETKIADI = SDR["YT_YETKIADI"].ToString();
                    USR_GRUPKODU = SDR["GRP_USRKODU"].ToString();
                    USR_GRUPADI = SDR["GRP_ADI"].ToString();
                    USR_DURUM = Convert.ToBoolean(SDR["USR_AKTIF"].ToString());
                }

                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                return;
            }

        }
        public static void S_GETDAHILI(string Id, ref string ChannelID, ref string ChannelNO)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM Channel WHERE Channel_ID=@Id", con);
                com.Parameters.AddWithValue("@Id", Id);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    ChannelID = SDR["Channel_ChannelID"].ToString();
                    ChannelNO = SDR["Channel_Number"].ToString();
                    // USR_EXTEN = SDR["Extension"].ToString();
                    // USR_MAIL = SDR["EMail"].ToString();


                }

                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                return;
            }

        }

        public static void S_GETOZELGUN(string Id, ref string Holiday_Date, ref string Holiday_Memo)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM Holiday WHERE Holiday_ID=@Id", con);
                com.Parameters.AddWithValue("@Id", Id);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    Holiday_Date = SDR["Holiday_Date"].ToString();
                    Holiday_Memo = SDR["Holiday_Memo"].ToString();
                    // USR_EXTEN = SDR["Extension"].ToString();
                    // USR_MAIL = SDR["EMail"].ToString();


                }

                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                return;
            }

        }

        public static void S_GETSIFRE(string USR_KULLANICIADI, string USR_SIFRE, ref bool durum)
        {
            SqlConnection con = new SqlConnection();
            try
            {

                Connection.OpenConnection(ref con);
                SqlCommand com = new SqlCommand("SELECT * FROM I_KULLANICI WHERE USR_KULLANICIADI=@USR_KULLANICIADI", con);
                com.Parameters.AddWithValue("@USR_KULLANICIADI", USR_KULLANICIADI);
                SqlDataReader SDR = com.ExecuteReader();
                if (SDR.Read())
                {

                    USR_SIFRE = SDR["USR_SIFRE"].ToString();
                    durum = true;

                }

                con.Close();
            }
            catch (SqlException exp)
            {
                con.Close();
                durum = false;
                return;
            }

        }

        public static DataTable S_DiskDurumGetir()
        {


            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = @"SELECT [Type]
                                  ,[intValue]
                                  ,[strValue]
                                  ,[Description]
                                  ,[intValue2]
                                  ,[intValue3]
                              FROM [dbo].[LocalCheckValues]
                              Where [Type] = 2 And [intValue] > -1
                              Order By [Type]";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }


        public static DataTable S_SistemDurumuGetir()
        {

            try
            {

                SqlConnection con = new SqlConnection();
                Connection.OpenConnection(ref con);
                System.Data.DataTable dtt = new System.Data.DataTable();
                string sorgu = @"SELECT [Type]
                                  ,[intValue]
                                  ,[strValue]
                                  ,[Description]
                                  ,[intValue2]
                                  ,[intValue3]
                              FROM [dbo].[LocalCheckValues]
                              Where [Type] = 1
                              Order By [Type]";
                SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
                daa.Fill(dtt);
                con.Close();
                return dtt;


            }
            catch (Exception ex)
            {

                throw ex;
            }


        }


        public static System.Data.DataTable S_KAYISORGULA(string StartDate, string EndDate, string Caller, string Called, string CallType, string DurStart, string DurEnd, string Id, string agent)
        {

            SqlConnection con = new SqlConnection();
            Connection.OpenConnection(ref con);
            System.Data.DataTable dtt = new System.Data.DataTable();
            string sorgu = @"Select   [vid] As [Ref],[extno],Case [call_type]
					When 1 Then 'Gelen Cagri'
					When 0 Then 'Cikan Cagri'
					Else 'Bilinmiyor'
				 End As [CallType]
				,[call_type] As [CType]
				,[id_num] As [Caller]
				,[phoneno] As [Called]
				,[recdate] As [StartDateTime]
				,[end_time] As [EndDateTime]
				,[connect_time] As [Duration]
				,'' As [StrDuration]
				,[filename] As [FilePath]
				,'' As [Description]
			  FROM [dbo].[voice]  where [recdate] >=@StartDate and [recdate] <=@EndDate	";

            //if (Caller != "")
            //{
            //    sorgu = sorgu + " AND Convert(Varchar, [id_num]) like @Caller";
            //}
            //if (Called != "")
            //{
            //    sorgu = sorgu + " AND Convert(Varchar, [phoneno]) like @Called";
            //}
            //if (CallType != "-1")
            //{
            //    sorgu = sorgu + " AND [call_type]=@CallType";
            //}
            //if (DurStart != "" && DurStart != "")
            //{
            //    sorgu = sorgu + " AND [connect_time]>= @DurStart and  [connect_time]<= @DurEnd ";
            //}
            //if (Id != "")
            //{
            //    sorgu = sorgu + " AND Convert(Varchar, [vid]) = @vid";
            //}
            //if (agent != "0")
            //{
            //    sorgu = sorgu + " AND Convert(Varchar, [chn_agentname]) = @chn_agentname";
            //}
            sorgu += " order  by [recdate] Desc";
            SqlDataAdapter daa = new SqlDataAdapter(sorgu, con);
            daa.SelectCommand.Parameters.AddWithValue("@StartDate", StartDate);
            daa.SelectCommand.Parameters.AddWithValue("@EndDate", EndDate);
            daa.SelectCommand.Parameters.AddWithValue("@Caller", "%" + Caller + "%");
            daa.SelectCommand.Parameters.AddWithValue("@Called", "%" + Called + "%");
            daa.SelectCommand.Parameters.AddWithValue("@CallType", CallType);
            daa.SelectCommand.Parameters.AddWithValue("@DurStart", DurStart);
            daa.SelectCommand.Parameters.AddWithValue("@DurEnd", DurEnd);
            daa.SelectCommand.Parameters.AddWithValue("@vid", Id);
            daa.SelectCommand.Parameters.AddWithValue("@chn_agentname", agent);
            daa.Fill(dtt);
            con.Close();
            return dtt;

        }
    }
}
