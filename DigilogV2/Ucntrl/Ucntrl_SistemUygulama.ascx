﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_SistemUygulama.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_SistemUygulama" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<h3>
    <i class="fa fa-phone-square"></i>Sistem Uygulama
</h3>

<div align="Center">
    <asp:Image ID="imgCurrentStatus" runat="server" Height="160px" ImageUrl="~/images/run.png" Width="200px" />
    <br />
    <asp:Label ID="lblDurum" runat="server" Text="Durum:"></asp:Label>
    <asp:Label ID="lblCurrentStatus" runat="server" Text="----"></asp:Label>
</div>
<dx:ASPxTimer ID="timerZaman" Interval="15000" runat="server" OnTick="timerZaman_Tick"></dx:ASPxTimer>
