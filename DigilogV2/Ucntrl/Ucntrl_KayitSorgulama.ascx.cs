﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_KayitSorgulama : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Session["dgRecordReports"] = null;
                Session["dgSummary"] = null;


                comboKayitSayisi.Items.Add("10");
                comboKayitSayisi.Items.Add("20");
                comboKayitSayisi.Items.Add("50");
                comboKayitSayisi.Items.Add("100");
                comboKayitSayisi.SelectedIndex = 0;

             
              

                //txtStartDate.DisplayFormatString = WebConfigurationManager.AppSettings["Datemode"];
                //txtEndDate.DisplayFormatString = WebConfigurationManager.AppSettings["Datemode"];

                txtStartDate.Date = DateTime.Now;
                txtEndDate.Date = DateTime.Now;
                txtStartTime.DateTime = Convert.ToDateTime(DateTime.Now.ToString("00:00:00"));
                txtEndTime.DateTime = Convert.ToDateTime(DateTime.Now.ToString("23:59:59"));
               
              
            }
           
            SORGULA();
        }
        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {

                    DevExpress.Web.ASPxMenu.MenuItem sil = this.ASPxMenu1.Items.FindByName("SIL");
                    sil.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }
        void SORGULA()
        {
            ASPxGridView1.DataSource = HelperDataLib.Select.S_KAYISORGULA(txtStartDate.Date.ToString(System.Configuration.ConfigurationManager.AppSettings["Datemode"].ToString()) + " " + txtStartTime.DateTime.ToString("HH:mm:ss"), txtEndDate.Date.ToString(System.Configuration.ConfigurationManager.AppSettings["Datemode"].ToString()) + " " + txtEndTime.DateTime.ToString("HH:mm:ss"), txtCaller.Text, txtCalled.Text, "", txtDurationStart.Text, txtDurationEnd.Text, txtId.Text, "");
            ASPxGridView1.DataBind();
        }

        protected void comboKayitSayisi_SelectedIndexChanged(object sender, EventArgs e)
        {
            ASPxGridView1.SettingsPager.PageSize = Convert.ToInt32(comboKayitSayisi.SelectedItem.Value);
            ASPxGridView1.DataBind();
        }

        protected void ASPxMenu1_ItemClick(object source, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
        {
            if (e.Item.Name == "ISLEM")
            {
                SORGULA();
            }
            if (e.Item.Name == "GRAFIK")
            {
                if (ASPxGridView1.VisibleRowCount > 0)
                {
                    Random random = new Random();
                    int value = random.Next(1, 10000);


                }
            }
            if (e.Item.Name == "EXCEL")
            {
                SORGULA();
                ASPxGridViewExporter1.GridViewID = "dgRecordReports";
                ASPxGridViewExporter1.FileName = "Rapor" + "_" + DateTime.Now;
                ASPxGridViewExporter1.WriteXlsxToResponse();

            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
          
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            
        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
    }
}