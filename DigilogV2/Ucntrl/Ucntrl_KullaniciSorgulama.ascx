﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_KullaniciSorgulama.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_KullaniciSorgulama" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<h3>
    <i class="fa fa-phone-square"></i>Log Verileri
</h3>
<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
    <div class="alert alert-success alert-dismissable" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
        İşleminiz başarı ile gerçekleşti.
    </div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
    <div class="alert alert-danger" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
    </div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
    <div class="alert alert-warning" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
    </div>
</asp:Panel>
<div style="width: 100%">
    <table style="width: 100%;">
        <tr>
            <td class="style1">
                <asp:Label ID="lblBaslamaTarihSaat" runat="server" Text="Başlama Tarih-Saat"></asp:Label>
            </td>
            <td class="style2">:</td>

            <td class="auto-style1">
                <dx:ASPxDateEdit ID="dateBaslamaTarih" runat="server" Theme="Moderno"
                    DisplayFormatString="dd.MM.yyyy">
                </dx:ASPxDateEdit>
            </td>
            <td class="style4">
                <dx:ASPxTimeEdit ID="dateBaslamaSaat" runat="server" Theme="Moderno"
                    Width="100px">
                </dx:ASPxTimeEdit>
            </td>
            <td class="style5">
                <asp:Label ID="lblBitisTarihSaat" runat="server" Text="Bitiş Tarih-Saat"></asp:Label>
            </td>
            <td class="style6">:</td>
            <td class="auto-style1">
                <dx:ASPxDateEdit ID="dateBitisTarih" runat="server" Theme="Moderno"
                    DisplayFormatString="dd.MM.yyyy">
                </dx:ASPxDateEdit>
            </td>
            <td>
                <dx:ASPxTimeEdit ID="dateBitisSaat" runat="server" Theme="Moderno"
                    Width="100px">
                </dx:ASPxTimeEdit>
            </td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Label ID="lblKullanicilar" runat="server" Text="Kullanıcılar"></asp:Label>
            </td>
            <td class="style2">:</td>
            <td class="auto-style1">
                <dx:ASPxComboBox ID="DropDownListUsers" runat="server" Theme="Moderno"
                    ValueType="System.Int32" TextField="USR_ID" ValueField="USR_ID"
                    Width="170px">
                    <Columns>
                        <dx:ListBoxColumn FieldName="USR_ISIM" Name="Ad" />
                        <dx:ListBoxColumn FieldName="USR_KULLANICIADI" Name="UserName" />
                    </Columns>
                </dx:ASPxComboBox>
            </td>
            <td class="style4">&nbsp;</td>
            <td class="style5">
                <asp:Label ID="lblIslem" runat="server" Text="İşlem"></asp:Label>
            </td>
            <td class="style6">:</td>
            <td class="auto-style1">
                <dx:ASPxComboBox ID="DropDownListProcess" runat="server" Theme="Moderno">
                    <Items>
                        <dx:ListEditItem Text="1" Value="1" />
                    </Items>
                </dx:ASPxComboBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">
                <asp:Label ID="lblResult" runat="server" Visible="true"></asp:Label>
            </td>
            <td class="style2">&nbsp;</td>
            <td class="style3" colspan="2">
                <%--   <dx:ASPxRadioButtonList ID="radioSecim" runat="server" RepeatDirection="Horizontal" SelectedIndex="0" Theme="Office2010Black">
                <Items>
                    <dx:ListEditItem Selected="True" Text="Merkez" Value="Merkez" />
                    <dx:ListEditItem Text="Seçili Şube" Value="Seçili Şube" />
                </Items>
            </dx:ASPxRadioButtonList>--%>
            </td>
            <td class="style5">&nbsp;</td>
            <td class="style6">&nbsp;</td>
            <td class="auto-style1">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>
<div style="width: 100%">
    <dx:ASPxMenu ID="ASPxMenu1" runat="server" AutoSeparators="RootOnly"
        EnableTheming="True" HorizontalAlign="Left" ItemAutoWidth="False"
        OnItemClick="ASPxMenu1_ItemClick" Theme="Moderno" Width="100%">
        <Items>
            <dx:MenuItem Name="SORGU" Text="Sorgula">
                <Image Height="16px" Url="~/images/sorgu.png">
                </Image>
            </dx:MenuItem>
            <dx:MenuItem Name="EXCEL" Text="Excel'e Aktar">
                <Image Height="16px" Url="~/images/excel.png">
                </Image>
            </dx:MenuItem>

        </Items>
    </dx:ASPxMenu>
</div>


<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
    EnableTheming="True" Theme="MetropolisBlue" Width="100%">
    <Columns>
        <dx:GridViewDataTextColumn Caption="Kullanıcı" FieldName="USR_KULLANICIADI"
            VisibleIndex="0" Width="10%" ShowInCustomizationForm="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="İşlem" FieldName="Islem" VisibleIndex="1"
            Width="10%" ShowInCustomizationForm="True">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn ShowInCustomizationForm="True" FieldName="Description" Width="70%"
            Caption="Açıklama" VisibleIndex="2">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn Caption="İşlem Tarihi" FieldName="ProcessDate"
            VisibleIndex="3" Width="10%" ShowInCustomizationForm="True">
        </dx:GridViewDataTextColumn>
    </Columns>
    <SettingsBehavior AllowFocusedRow="True" />
    <SettingsPager>
        <Summary Text="Sayfa {0} / {1} ({2} veri)" />
    </SettingsPager>
    <Settings ShowFilterRow="True" />
    <SettingsText EmptyDataRow="Veri yok..." />
    <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
</dx:ASPxGridView>
<dx:ASPxGridViewExporter ID="ASPxGridViewExporterKullanici" runat="server" GridViewID="ASPxGridView1">
</dx:ASPxGridViewExporter>
