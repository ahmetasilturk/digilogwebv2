﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelperDataLib;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_YeniDepartman : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["id"] != null && Request.QueryString["new"] == "False")
                    {
                        Session["kullanicisession"] = HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString());
                        DETAY(HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()));
                    }
                    if (Request.QueryString["new"] == "True")
                    {
                        Session["kullanicisession"] = null;
                    }
                    YETKIKONTROL();
                    //HttpContext.Current.Cache["CacheDataTableYetki"] = HelperDataLib.Select.S_YETKI_YUKLE();
                    //HttpContext.Current.Cache["CacheDataTableGrup"] = HelperDataLib.Select.S_GRUP_LISTESI();
                }
            }
            YETKIYUKLE();
        }
        void DETAY(string id)
        {
            string USR_ADI = ""; string USR_SIFRE = ""; string USR_YETKIKODU = ""; string USR_YETKIADI = ""; string USR_GRUPKODU = ""; string USR_GRUPADI = ""; bool USR_DURUM = false; string USR_DEPARTMAN = ""; string USERNAME = "";
            HelperDataLib.Select.S_USERS_GETDETAY(id, ref USERNAME, ref USR_ADI, ref USR_SIFRE, ref USR_YETKIKODU, ref USR_YETKIADI, ref USR_GRUPKODU, ref USR_GRUPADI,ref USR_DEPARTMAN, ref USR_DURUM);
            txt_departmanadi.Text = USR_DEPARTMAN;
            //txt_isim.Text = USR_ADI;
            //t_exten.Text = USR_EXTEN;
            //t_mail.Text = USR_MAIL;
            //t_telefon.Text = USR_TELEFON;
            txt_sube.Text = USR_YETKIADI;
            Session["yetkiid"] = USR_YETKIKODU;
            // txt_grup.Text = USR_GRUPADI;
            Session["grupid"] = USR_GRUPKODU;
            //aktifpasif.Checked = USR_DURUM;
            // txt_sifre.Text = USR_SIFRE;


        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
        void YETKIKONTROL()
        {
            bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
            HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
            if (YAZ == true)
            {

                ASPxButton1.Enabled = true;
                ASPxButton3.Enabled = true;
                ASPxButton4.Enabled = true;
            }
            if (OKU == false)
            {
                Response.Redirect("~/Default.aspx");
            }

        }

        void YETKIYUKLE()
        {
            try
            {
                ASPxGridView2.DataSource = HelperDataLib.Select.S_SUBE_YUKLE();
                ASPxGridView2.DataBind();
                //ASPxGridView3.DataSource = HttpContext.Current.Cache["CacheDataTableGrup"];
                //ASPxGridView3.DataBind();
            }
            catch
            { return; }
        }
        void TEMIZLE()
        {
            txt_departmanadi.Text = "";
            //txt_soyisim.Text = "";
            //txt_isim.Text = "";
            //txt_departman.Text = "";
            txt_sube.Text = "";
            Session["subeid"] = null;
            // txt_grup.Text = "";
            //Session["grupid"] = null;
            // aktifpasif.Checked = false;
            Session["kullanicisession"] = null;
            // txt_sifre.Text = "";
            // t_telefon.Text = "";

        }
        void KAYIT(ref bool durum)
        {
            MESAJKAPAT();
            //kayıt
            if (Session["kullanicisession"] == null)
            {
                if (txt_sube.Text != "" && txt_departmanadi.Text != "")
                {
                    HelperDataLib.Insert.I_DEPARTMAN(txt_departmanadi.Text, Int32.Parse(Session["subeid"].ToString()), ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            else
            {
                if (txt_sube.Text != "" && txt_departmanadi.Text != "")
                {
                    //HelperDataLib.Update.U_KULLANICI(Session["kullanicisession"].ToString(), t_kullaniciadi.Text,t_isimsoyisim.Text,t_exten.Text, t_mail.Text,t_telefon.Text, t_sifre.Text, Session["yetkiid"].ToString(), Session["grupid"].ToString(), aktifpasif.Checked, ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            Response.Redirect("~/DepartmanYonetimi.aspx");
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            //yeni
            TEMIZLE();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            //kaydet
            bool durum = false;
            KAYIT(ref durum);
            if (durum == true)
            {
                TEMIZLE();
                succes.Visible = true;
            }
        }
        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            //vazgeç
            TEMIZLE();
            Response.Redirect("~/DepartmanYonetimi.aspx");
        }

        protected void ImageButton1_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            string[] arg = new string[2];
            arg = doc.ToString().Split(';');
            Session["subeid"] = arg[0];
            txt_sube.Text = arg[1];
            ASPxPopupControl2.ShowOnPageLoad = false;
        }

    }
}