﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_DepolamaBirimi : System.Web.UI.UserControl
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Page.IsPostBack)
                {
                    dtlstStorageMedia.DataBind();
                   
                }
            }
            catch
            {
                return;
            }
        }

        public List<LocalValue> GetLocalCheckValues()
        {
            List<LocalValue> lvalues = new List<LocalValue>();
            try
            {
                #region LCV
                DataTable dt = HelperDataLib.Select.S_DiskDurumGetir();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dtrw in dt.Rows)
                    {
                        LocalValue lval = new LocalValue(dtrw);
                        lvalues.Add(lval);
                    }
                }
                #endregion
            }
            catch (Exception)
            { }
            return lvalues;
        }

        public List<StorageDisc> GetDBCheckDiscs()
        {
            List<StorageDisc> storageMedias = new List<StorageDisc>();
            foreach (LocalValue lVal in GetLocalCheckValues())
            {
                try
                {
                    StorageDisc storageMedia = new StorageDisc();
                    storageMedia.Name = lVal.strValue;
                    storageMedia.Size = lVal.intValue;
                    storageMedia.FreeSpace = lVal.intValue2;
                    decimal size = Convert.ToDecimal(storageMedia.Size) / (1024 * 1024 * 1024);
                    decimal freeSize = Convert.ToDecimal(storageMedia.FreeSpace) / (1024 * 1024 * 1024);
                    decimal usage = ((Convert.ToDecimal(storageMedia.Size) - Convert.ToDecimal(storageMedia.FreeSpace)) * 100) / Convert.ToDecimal(storageMedia.Size);
                    decimal free = 100 - usage;
                    storageMedia.Usage = usage;
                    storageMedia.Sz = size.ToString("0.##") + " GB";
                    storageMedia.FrSpc = freeSize.ToString("0.##") + " GB";
                    storageMedia.Usg = "%" + usage.ToString("0.##");
                    storageMedia.Free = "%" + free.ToString("0.##");
                    storageMedia.Wdh = Convert.ToInt32(usage.ToString("0")) + Convert.ToInt32(usage.ToString().Substring(0, 1));
                    storageMedia.Wdth = 110 - storageMedia.Wdh;
                    storageMedias.Add(storageMedia);
                }
                catch { }
            }
            return storageMedias;
        }

        protected void dtlstStorageMedia_DataBound(object sender, EventArgs e)
        {



        }

        protected int ConvertToImageSize(object imageSize)
        {
            int i = 0;
            if (imageSize != null)
            {
                i = Convert.ToInt32(imageSize);

            }

            return i;

        }

        protected void dtlstStorageMedia_DataBinding(object sender, EventArgs e)
        {
            dtlstStorageMedia.DataSource = GetDBCheckDiscs();
        }


    }
}