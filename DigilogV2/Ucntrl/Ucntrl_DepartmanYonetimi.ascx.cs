﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_DepartmanYonetimi : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            YETKIKONTROL();
            YUKLE();
        }
        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {

                    DevExpress.Web.ASPxMenu.MenuItem sil = this.ASPxMenu1.Items.FindByName("SIL");
                    sil.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }
        void YUKLE()
        {
            ASPxGridView1.DataSource = HelperDataLib.Select.S_DEPARTMAN_YUKLE();
            ASPxGridView1.DataBind();
        }

        protected void ASPxMenu1_ItemClick(object source, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
        {
            MESAJKAPAT();
            if (e.Item.Name == "ISLEM")
            {
                Session["yetkiid"] = null;
                Session["kulmno"] = null;
                Session["kullanicisession"] = null;
                Response.Redirect("YeniDepartman.aspx?new=" + true);
            }
            if (e.Item.Name == "SIL")
            {
                bool durum = false;
                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        string departman_id = ASPxGridView1.GetRowValues(i, new string[] { "ID" }).ToString();
                        HelperDataLib.Delete.D_DEPARTMAN(departman_id, ref durum);
                        if (durum == true)
                        {
                            succes.Visible = true;
                        }
                        if (durum == false)
                        {
                            error.Visible = true;
                        }
                    }


                }
                if (durum == true)
                {
                    YUKLE();
                }

            }
            if (e.Item.Name == "YENILE")
            {
                YUKLE();
            }
        }

        protected void ASPxButton1_Command(object sender, CommandEventArgs e)
        {
            //detay
            string doc = e.CommandArgument.ToString();
            Response.Redirect("YeniDepartman.aspx?id=" + HelperDataLib.Encrypt.UrlEncrypt(doc.Trim()) + "&&" + "new=" + false);

        }

        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
    }
}