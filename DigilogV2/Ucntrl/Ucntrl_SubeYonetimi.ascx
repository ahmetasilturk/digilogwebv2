﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_SubeYonetimi.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_SubeYonetimi" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<h3>
    <i class="fa fa-share-alt"></i>Şube Yönetimi
</h3>
<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
    <div class="alert alert-success alert-dismissable" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
        İşleminiz başarı ile gerçekleşti.
    </div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
    <div class="alert alert-danger" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
    </div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
    <div class="alert alert-warning" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
    </div>
</asp:Panel>
<div style="width: 100%">
    <dx:ASPxMenu ID="ASPxMenu1" runat="server" AutoSeparators="RootOnly"
        EnableTheming="True" HorizontalAlign="Left" ItemAutoWidth="False"
        OnItemClick="ASPxMenu1_ItemClick" Theme="Moderno" Width="100%">
        <Items>
            <dx:MenuItem Name="ISLEM" Text="Yeni Şube">
                <Image Height="16px" Url="~/images/Add_16x16.png">
                </Image>
            </dx:MenuItem>
            <dx:MenuItem Name="SIL" Text="Sil">
                <Image Height="16px" Url="~/images/DeleteList2_16x16.png">
                </Image>
            </dx:MenuItem>
            <dx:MenuItem Name="YENILE" Text="Yenile">
                <Image Height="16px" Url="~/images/Refresh_16x16.png">
                </Image>
            </dx:MenuItem>
        </Items>
    </dx:ASPxMenu>
</div>


<dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
    EnableTheming="True" Theme="MetropolisBlue" Width="100%" KeyFieldName="BranchCode">
    <Columns>
      
         <dx:GridViewCommandColumn Caption="Seç" ShowSelectCheckbox="True" 
            VisibleIndex="0" Width="2%">
        </dx:GridViewCommandColumn>
        
        <dx:GridViewDataTextColumn FieldName="BranchCode" Caption="Şube Kodu" VisibleIndex="0">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Name" Caption="İsim" VisibleIndex="1">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="Channel" Caption="Kanal" VisibleIndex="3">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataTextColumn FieldName="IpAddress" Caption="Ip Adresi" VisibleIndex="4">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataCheckColumn FieldName="IsActive" Caption="Aktif" VisibleIndex="2">
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataTextColumn FieldName="SSName" Caption="Sql Sunucu Adı" VisibleIndex="6">
        </dx:GridViewDataTextColumn>
        <dx:GridViewDataCheckColumn FieldName="SqlExpress" Caption="Sql Express" VisibleIndex="5">
        </dx:GridViewDataCheckColumn>
        <dx:GridViewDataTextColumn FieldName="DataBase" Caption="Veritabanı" VisibleIndex="7">
        </dx:GridViewDataTextColumn>
      
         <dx:GridViewDataTextColumn Caption=" "  VisibleIndex="8" Width="2%" FieldName="USR_ID">
             <DataItemTemplate>
                 <dx:ASPxButton ID="ASPxButton1" runat="server" OnCommand="ASPxButton1_Command" CommandArgument="<%# Bind('BranchCode')%>" RenderMode="Link" ToolTip="Detay">
                     <Image Url="~/images/Forward_16x16.png">
                     </Image>
                 </dx:ASPxButton>
            </DataItemTemplate>
        </dx:GridViewDataTextColumn>
    </Columns>

    <SettingsBehavior AllowFocusedRow="True" />
    <SettingsPager>
        <Summary Text="Sayfa {0} / {1} ({2} veri)" />
    </SettingsPager>
    <Settings ShowFilterRow="True" />
    <SettingsText EmptyDataRow="Veri yok..." />
    <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
</dx:ASPxGridView>
