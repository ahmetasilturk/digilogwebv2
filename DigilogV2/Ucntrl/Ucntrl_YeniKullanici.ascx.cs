﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelperDataLib;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_YeniKullanici : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["id"] != null && Request.QueryString["new"] == "False")
                    {
                        Session["kullanicisession"] = HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString());
                        DETAY(HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()));
                    }
                    if (Request.QueryString["new"] == "True")
                    {
                        Session["kullanicisession"] = null;
                    }
                    YETKIKONTROL();
                    HttpContext.Current.Cache["CacheDataTableYetki"] = HelperDataLib.Select.S_YETKI_YUKLE();
                    HttpContext.Current.Cache["CacheDataTableGrup"] = HelperDataLib.Select.S_GRUP_LISTESI();
                }
            }
            YETKIYUKLE();
        }
        void DETAY(string id)
        {
            string USR_ADI = ""; string USR_SIFRE = ""; string USR_YETKIKODU = ""; string USR_YETKIADI = ""; string USR_GRUPKODU = ""; string USR_GRUPADI = ""; bool USR_DURUM = false; string USR_DEPARTMAN = ""; string USERNAME = "";
            HelperDataLib.Select.S_USERS_GETDETAY(id, ref USERNAME, ref USR_ADI, ref USR_SIFRE, ref USR_YETKIKODU, ref USR_YETKIADI, ref USR_GRUPKODU, ref USR_GRUPADI,ref USR_DEPARTMAN, ref USR_DURUM);
            txt_kullaniciadi.Text = USERNAME;
            txt_isim.Text = USR_ADI;
            //t_exten.Text = USR_EXTEN;
            //t_mail.Text = USR_MAIL;
            //t_telefon.Text = USR_TELEFON;
            txt_departman.Text = USR_DEPARTMAN;
            txt_yetki.Text = USR_YETKIADI;
            Session["yetkiid"] = USR_YETKIKODU;
            txt_grup.Text = USR_GRUPADI;
            Session["grupid"] = USR_GRUPKODU;
            aktifpasif.Checked = USR_DURUM;
            txt_sifre.Text = USR_SIFRE;



        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
        void YETKIKONTROL()
        {
            bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
            HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
            if (YAZ == true)
            {

                ASPxButton1.Enabled = true;
                ASPxButton3.Enabled = true;
                ASPxButton4.Enabled = true;
            }
            if (OKU == false)
            {
                Response.Redirect("~/Default.aspx");
            }

        }

        void YETKIYUKLE()
        {
            try
            {
                ASPxGridView1.DataSource = HelperDataLib.Select.S_DEPARTMAN_YUKLE();
                ASPxGridView1.DataBind();
                ASPxGridView2.DataSource = HttpContext.Current.Cache["CacheDataTableYetki"];
                ASPxGridView2.DataBind();
                ASPxGridView3.DataSource = HttpContext.Current.Cache["CacheDataTableGrup"];
                ASPxGridView3.DataBind();


            }
            catch
            { return; }
        }
        void TEMIZLE()
        {
            txt_kullaniciadi.Text = "";
            txt_soyisim.Text = "";
            txt_isim.Text = "";
            txt_departman.Text = "";
            txt_yetki.Text = "";
            Session["yetkiid"] = null;
            txt_grup.Text = "";
            Session["grupid"] = null;
            aktifpasif.Checked = false;
            Session["kullanicisession"] = null;
            txt_sifre.Text = "";
            // t_telefon.Text = "";

        }
        void KAYIT(ref bool durum)
        {
            MESAJKAPAT();
            //kayıt
            if (Session["kullanicisession"] == null)
            {
                if (txt_grup.Text != "" && txt_yetki.Text != "" && txt_sifre.Text != "" && txt_kullaniciadi.Text != "" && txt_isim.Text != "" && txt_soyisim.Text != "")
                {
                    //int kullaniciid = HelperDataLib.Select.S_GETKULLANICIID();
                    HelperDataLib.Insert.I_KULLANICI(txt_kullaniciadi.Text, txt_isim.Text, txt_soyisim.Text, txt_sifre.Text, Int32.Parse(Session["yetkiid"].ToString()), Int32.Parse(Session["grupid"].ToString()), Int32.Parse(Session["depid"].ToString()), aktifpasif.Checked, ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            else
            {
                if (txt_grup.Text != "" && txt_yetki.Text != "" && txt_sifre.Text != "" && txt_kullaniciadi.Text != "" && txt_isim.Text != "" && txt_soyisim.Text != "")
                {

                    string mesaj = Session["yetkiid"].ToString() + "-" + Session["grupid"].ToString() + "-" + Session["depid"].ToString();

                    HelperDataLib.Update.U_KULLANICI(txt_kullaniciadi.Text, txt_isim.Text, txt_soyisim.Text, txt_sifre.Text, Int32.Parse(Session["yetkiid"].ToString()), Int32.Parse(Session["grupid"].ToString()), Int32.Parse(Session["depid"].ToString()), aktifpasif.Checked, ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }

            Response.Redirect("~/KullaniciListesi.aspx");
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            //yeni
            TEMIZLE();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            //kaydet
            bool durum = false;
            KAYIT(ref durum);
            if (durum == true)
            {
                TEMIZLE();
                succes.Visible = true;
            }
        }
        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            //vazgeç
            TEMIZLE();
            Response.Redirect("~/KullaniciListesi.aspx");
        }
        protected void ImageButton3_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            string[] arg = new string[2];
            arg = doc.ToString().Split(';');
            Session["depid"] = arg[0];
            txt_departman.Text = arg[1];
            ASPxPopupControl1.ShowOnPageLoad = false;
        }

        protected void ImageButton2_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            string[] arg = new string[2];
            arg = doc.ToString().Split(';');
            Session["grupid"] = arg[0];
            txt_grup.Text = arg[1];
            ASPxPopupControl3.ShowOnPageLoad = false;
        }
        protected void ImageButton1_Command(object sender, CommandEventArgs e)
        {
            string doc = e.CommandArgument.ToString();
            string[] arg = new string[2];
            arg = doc.ToString().Split(';');
            Session["yetkiid"] = arg[0];
            txt_yetki.Text = arg[1];
            ASPxPopupControl2.ShowOnPageLoad = false;
        }

    }
}