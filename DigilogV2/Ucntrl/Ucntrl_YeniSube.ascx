﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_YeniSube.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_YeniSube" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style type="text/css">
    .style5 {
        width: 1px;
    }

    .style7 {
        width: 90px;
    }

    .style8 {
        width: 50%;
    }

    .style10 {
        width: 93px;
    }

    .style13 {
    }

    .style15 {
        width: 33%;
    }

    </style>
<script type="text/javascript">
    function popup() {
        personelpop.Show();
    }
    function yetki() {
        personelyetki.Show();
    }
    function grup() {
        popgrup.Show();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <h3>
            <i class="fa fa-users"></i>Yeni Şube İşlemi
        </h3>
        <asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
            <div class="alert alert-success alert-dismissable" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
                İşleminiz başarı ile gerçekleşti.
            </div>
        </asp:Panel>
        <asp:Panel ID="error" runat="server" Visible="false" Width="100%">
            <div class="alert alert-danger" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
            </div>
        </asp:Panel>
        <asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
            <div class="alert alert-warning" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
            </div>
        </asp:Panel>

        <table style="width: 100%;">
            <tr>
                <td class="style8" valign="top">
                    <table style="width: 100%;">

                        <tr>

                            <td class="style7">
                                <dx:ASPxLabel ID="lblSubeDurumu" runat="server" Text="Şube Durumu">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxCheckBox ID="checkAktif" runat="server" Text="Aktif"
                                    Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblSubeKodu" runat="server" Text="Şube Kodu">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtSubeKodu" runat="server" Width="100%" NullText="Zorunlu alan..."
                                    Theme="Moderno">
                                    <ValidationSettings ValidationGroup="kaydet">
                                        <RegularExpression ErrorText="Lütfen Sayı Giriniz." ValidationExpression="^\d+$" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                         <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblSubeIsmi" runat="server" Text="Şube İsmi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtSubeIsmi" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblSubeKanalBilgisi" runat="server" Text="Şube Kanal Bilgisi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtSubeKanal" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                    <ValidationSettings ValidationGroup="kaydet">
                                        <RegularExpression ErrorText="Lütfen Sayı Giriniz." ValidationExpression="^\d+$" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblIpAdresi" runat="server" Text="Ip Adresi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtIpAddress" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblSunucuSqlExpressmi" runat="server" Text="Sunucu Sql Express mi?">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxCheckBox ID="checkSqlExpress" runat="server" CheckState="Unchecked"
                                    Text="SQl Express" Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>
                        </tr>
                       <%-- <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblDisIpAdresi" runat="server" Text="Dış İp Adresi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxCheckBox ID="checkDısIP" runat="server" Text="Dış İp"
                                    OnCheckedChanged="checkDısIP_CheckedChanged" AutoPostBack="True"
                                    Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                                <dx:ASPxTextBox ID="txtDisIp" runat="server" Width="100%" NullText="Zorunlu alan..." Enabled="False" Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>--%>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblSqlServerAdi" runat="server" Text="Sql Server Adı">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtSqlServerName" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblVeritabani" runat="server" Text="Veritabanı">
                                </dx:ASPxLabel>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <dx:ASPxTextBox ID="txtDatabase" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                      <%--  <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblYedekIpAdresi" runat="server" Text="Yedek İp Adresi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtYedekIp" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblYedekSqlExpress" runat="server">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxCheckBox ID="checkYedekSqlExpress" runat="server"
                                    Text="Yedek Sql Express" Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblYedekTablo" runat="server" Text="Yedek Tablo">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtYedekTablo" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblZamanAsimi" runat="server" Text="Zaman Aşımı">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtZamanAsimi" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblTopluIndirmeYolu" runat="server" Text="Toplu İndirme Yolu">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtTopluIndirmeYolu" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblSmtpAdres" runat="server" Text="Smtp Adres">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtSmtpAdres" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblMailKullaniciAdi" runat="server" Text="Mail Kullanıcı Adı">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtMailKullaniciAdi" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblGonderenMailAdres" runat="server" Text="Gönderen Mail Adres">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txtGonderenMAilAdres" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>

                        </tr>

                        <tr>
                            <td class="style7">
                                <dx:ASPxLabel ID="lblMailOnceligi" runat="server" Text="Mail Önceliği">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>

                                <dx:ASPxComboBox ID="comboMailOnceligi" runat="server" Theme="Moderno" SelectedIndex="0">
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="Düşük" Value="Low" />
                                        <dx:ListEditItem Selected="True" Text="Normal" Value="Normal" />
                                        <dx:ListEditItem Selected="True" Text="Yüksek" Value="High" />
                                    </Items>
                                </dx:ASPxComboBox>

                            </td>

                        </tr>--%>
                    </table>
                      <table style="width: 100%;">
                        <tr>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton4" runat="server" OnClick="ASPxButton4_Click"
                                    Text="Yeni" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click"
                                    Text="Kaydet" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>

                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton3" runat="server" OnClick="ASPxButton3_Click"
                                    Text="Vazgeç" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="style8" valign="top" align="right">
                    <table style="width: 100%;">
                       
                       <%-- <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblYedektenSorgulama" runat="server" Text="Yedekten Sorguama">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left">
                                <dx:ASPxCheckBox ID="checkYedeklemeTablosu" runat="server"
                                    CheckState="Unchecked" Text="Aktif (Yedekleme Tablosu Kullan)"
                                    Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblYedekSqlServerName" runat="server" Text="Yedek Sql Server Name">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtYedekSqlServerName" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblYedekDatabase" runat="server" Text="Yedek Database">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtYedekDatabase" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblYedeklemeYolu" runat="server" Text="Yedekleme Yolu">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtYedeklemeYolu" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblVirtualPath" runat="server" Text="Sanal Dizin">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtSanalDizin" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblTopluIndirmeSanalDizin" runat="server" Text="Toplu İndirme Sanal Dizin">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtTopluIndirmeSanalDizin" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblSmtpPort" runat="server" Text="Smtp Port">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtSmtpPort" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                    <ValidationSettings ValidationGroup="kaydet">
                                        <RegularExpression ErrorText="Lütfen Sayı Giriniz." ValidationExpression="^\d+$" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblMailSifre" runat="server" Text="Mail Şifre">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtMailSifre" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <dx:ASPxLabel ID="lblGidenMAilAdres" runat="server" Text="Uyarı Maili Giden Mail Adresi">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                           <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txtUyariGidenMailAdresi" runat="server" Width="100%" NullText="Zorunlu alan..." Theme="Moderno">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <dx:ASPxLabel ID="lblDigerSecenekler" runat="server" Text="Diğer Seçenekler">
                                </dx:ASPxLabel>
                            </td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxCheckBox ID="checkSSL" runat="server" CheckState="Unchecked" Text="SSL"
                                    Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                                <dx:ASPxCheckBox ID="checkCredential" runat="server" CheckState="Unchecked"
                                    Text="Credential" Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>
                        </tr>--%>

                        <tr>
                            <td class="style10" align="left">&nbsp;</td>
                            <td class="style5">&nbsp;</td>
                            <td align="left" colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                  
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>







