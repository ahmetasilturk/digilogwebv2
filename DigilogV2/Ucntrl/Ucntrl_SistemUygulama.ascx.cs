﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_SistemUygulama : System.Web.UI.UserControl
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                CheckApplication();

            }
        }

        public void CheckApplication()
        {
           
            string calisanMesaj, calismayanMesaj = null;
            calisanMesaj = "Sistem Çalışıyor.";
            calismayanMesaj = "Sistem Çalışmıyor!!!";

            Boolean isRunning = false;
            foreach (LocalValue lVal in GetLocalCheckValues())
            {
                try
                {
                    if (lVal.Type == 1)
                        isRunning = lVal.intValue == 1 ? true : false;
                }
                catch { }
            }

            if (isRunning)
            {
                imgCurrentStatus.ImageUrl = "~/images/database-on.png";

                lblCurrentStatus.Text = calisanMesaj;
            }
            else
            {
                imgCurrentStatus.ImageUrl = "~/images/database-off.png";
                lblCurrentStatus.Text = calismayanMesaj;
            }
        }

        public List<LocalValue> GetLocalCheckValues()
        {
            List<LocalValue> lvalues = new List<LocalValue>();
            try
            {
                #region LCV
                DataTable dt = HelperDataLib.Select.S_SistemDurumuGetir();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dtrw in dt.Rows)
                    {
                        LocalValue lval = new LocalValue(dtrw);
                        lvalues.Add(lval);
                    }
                }
                #endregion
            }
            catch (Exception)
            { }
            return lvalues;
        }

        protected void timerZaman_Tick(object sender, EventArgs e)
        {
            CheckApplication();
        }


    }
}