﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelperDataLib;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_YeniDahili : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["id"] != null && Request.QueryString["new"] == "False")
                    {
                        Session["channelid"] = Request.QueryString["id"].ToString();
                        DETAY(Request.QueryString["id"].ToString());
                    }
                    if (Request.QueryString["new"] == "True")
                    {
                        Session["channelid"] = null;
                    }
                   // YETKIKONTROL();
                    //HttpContext.Current.Cache["CacheDataTableYetki"] = HelperDataLib.Select.S_YETKI_YUKLE();
                    //HttpContext.Current.Cache["CacheDataTableGrup"] = HelperDataLib.Select.S_GRUP_LISTESI();
                }
            }
            //YETKIYUKLE();
        }
        void DETAY(string id)
        {
            string ChannelID = ""; string ChannelNO = "";
            HelperDataLib.Select.S_GETDAHILI(id, ref ChannelID, ref ChannelNO);
            txt_dahili.Text = ChannelNO;
            txt_kanal.Text = ChannelID;
            //t_exten.Text = USR_EXTEN;
            //t_mail.Text = USR_MAIL;
            //t_telefon.Text = USR_TELEFON;
            //txt_kanal_tipi.Text = USR_DEPARTMAN;
            //txt_yetki.Text = USR_YETKIADI;
            //Session["yetkiid"] = USR_YETKIKODU;
            //txt_grup.Text = USR_GRUPADI;
            //Session["grupid"] = USR_GRUPKODU;
            //aktifpasif.Checked = USR_DURUM;
            //txt_sifre.Text = USR_SIFRE;



        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
        void YETKIKONTROL()
        {
            bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
            HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
            if (YAZ == true)
            {

                ASPxButton1.Enabled = true;
                ASPxButton3.Enabled = true;
                ASPxButton4.Enabled = true;
            }
            if (OKU == false)
            {
                Response.Redirect("~/Default.aspx");
            }

        }

      
        void TEMIZLE()
        {
            txt_dahili.Text = "";
            //txt_soyisim.Text = "";
            txt_kanal.Text = "";
            //txt_kanal_tipi.Text = "";
            //txt_yetki.Text = "";
            //Session["yetkiid"] = null;
            //txt_grup.Text = "";
            //Session["grupid"] = null;
            //aktifpasif.Checked = false;
            Session["channelid"] = null;
            //txt_sifre.Text = "";
            // t_telefon.Text = "";

        }
        void KAYIT(ref bool durum)
        {
            MESAJKAPAT();
            //kayıt
            if (Session["channelid"] == null)
            {
                if (txt_dahili.Text != "" && txt_kanal.Text != "")
                {
                    //int kullaniciid = HelperDataLib.Select.S_GETKULLANICIID();
                    HelperDataLib.Insert.I_DAHILI(txt_kanal.Text, txt_dahili.Text, ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            else
            {
                if (txt_dahili.Text != "" && txt_kanal.Text != "")
                {


                    HelperDataLib.Update.U_DAHILI(Int32.Parse(Session["channelid"].ToString()), txt_kanal.Text, txt_dahili.Text, ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }

            Response.Redirect("~/DahiliYonetimi.aspx");
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            //yeni
            TEMIZLE();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            //kaydet
            bool durum = false;
            KAYIT(ref durum);
            if (durum == true)
            {
                TEMIZLE();
                succes.Visible = true;
            }
        }
        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            //vazgeç
            TEMIZLE();
            Response.Redirect("~/DahiliYonetimi.aspx");
        }
        //protected void ImageButton3_Command(object sender, CommandEventArgs e)
        //{
        //    string doc = e.CommandArgument.ToString();
        //    string[] arg = new string[2];
        //    arg = doc.ToString().Split(';');
        //    Session["channelid"] = arg[0];
        //    txt_kanal_tipi.Text = arg[1];
        //    ASPxPopupControl1.ShowOnPageLoad = false;
        //}



    }
}