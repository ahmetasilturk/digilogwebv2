﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_YeniOzelGun.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_YeniOzelGun" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style type="text/css">
    .style5 {
        width: 1px;
    }

    .style7 {
        width: 90px;
    }

    .style8 {
        width: 50%;
    }

    .style10 {
        width: 93px;
    }

    .style13 {
    }

    .style15 {
        width: 33%;
    }

    .auto-style1 {
        width: 90px;
        height: 31px;
    }

    .auto-style2 {
        width: 1px;
        height: 31px;
    }

    .auto-style3 {
        height: 31px;
    }
</style>
<script type="text/javascript">
    //function popup() {
    //    kanaltipi.Show();
    //}
    //function yetki() {
    //    personelyetki.Show();
    //}
    //function grup() {
    //    popgrup.Show();
    //}
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <h3>
            <i class="fa fa-magic"></i>Yeni Özel Gün İşlemi
        </h3>
        <asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
            <div class="alert alert-success alert-dismissable" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
                İşleminiz başarı ile gerçekleşti.
            </div>
        </asp:Panel>
        <asp:Panel ID="error" runat="server" Visible="false" Width="100%">
            <div class="alert alert-danger" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
            </div>
        </asp:Panel>
        <asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
            <div class="alert alert-warning" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
            </div>
        </asp:Panel>
      
        <table style="width: 100%;">
            <tr>
                <td class="style8" valign="top">
                    <table style="width: 100%;">
                      
                        <tr>
                            <td class="auto-style1">
                                <asp:Label ID="Label32" runat="server" Text="Özel Gün"></asp:Label>
                                *</td>
                            <td class="auto-style2">:</td>
                            <td class="auto-style3">
                                <dx:ASPxDateEdit ID="txt_ozelgun" runat="server" Theme="Moderno" Width="100%">
                               
                                </dx:ASPxDateEdit>
                            </td>
                        </tr>
                         <tr>
                            <td class="style7">
                                <asp:Label ID="Label4" runat="server" Text="Açıklama"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txt_aciklama" runat="server" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">&nbsp;</td>
                            <td class="style5">&nbsp;</td>
                            <td>
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="center" class="style15">
                                            <dx:ASPxButton ID="ASPxButton4" runat="server" OnClick="ASPxButton4_Click" Text="Yeni" Theme="Moderno" Width="100%">
                                            </dx:ASPxButton>
                                        </td>
                                        <td align="center" class="style15">
                                            <dx:ASPxButton ID="ASPxButton1" runat="server"  OnClick="ASPxButton1_Click" Text="Kaydet" Theme="Moderno" Width="100%">
                                            </dx:ASPxButton>
                                        </td>
                                        <td align="center" class="style15">
                                            <dx:ASPxButton ID="ASPxButton3" runat="server"  OnClick="ASPxButton3_Click" Text="Vazgeç" Theme="Moderno" Width="100%">
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="style8" valign="top" align="right">
                    &nbsp;</td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>







