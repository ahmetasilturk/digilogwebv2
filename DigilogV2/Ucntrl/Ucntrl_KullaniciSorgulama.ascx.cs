﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_KullaniciSorgulama : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //YETKIKONTROL();
            if (!Page.IsPostBack)
            {
                dateBaslamaTarih.Date = DateTime.Today;
                dateBitisTarih.Date = DateTime.Today;
                dateBaslamaSaat.Text = "00:00:00";
                dateBitisSaat.DateTime = Convert.ToDateTime(DateTime.Now.ToString("23:00:00"));
                DropDownListUsers.SelectedIndex = 0;
                DropDownListProcess.SelectedIndex = 0;
                DropDownListUsers.DataSource = HelperDataLib.Select.S_USER_LISTESI();
                DropDownListUsers.DataBind();
                YUKLE();
            }
           
          
        }
        void YETKIKONTROL()
        {

            try
            {
                bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
                HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
                if (SIL == true)
                {

                    DevExpress.Web.ASPxMenu.MenuItem sil = this.ASPxMenu1.Items.FindByName("SIL");
                    sil.Visible = true;
                }
                if (OKU == false)
                {
                    Response.Redirect("~/Default.aspx");
                }
            }
            catch
            {
                return;
            }
        }
        void YUKLE()
        {
            DateTime basTar = Convert.ToDateTime(dateBaslamaTarih.Date.ToString("yyyy-MM-dd") + " " + dateBaslamaSaat.DateTime.ToString("HH:mm:ss"));
            DateTime bitTar = Convert.ToDateTime(dateBitisTarih.Date.ToString("yyyy-MM-dd") + " " + dateBitisSaat.DateTime.ToString("HH:mm:ss"));
            string select_user = DropDownListUsers.Value.ToString();
            //string select_process = DropDownListProcess.Value.ToString();

            ASPxGridView1.DataSource = HelperDataLib.Select.S_LOG_YUKLE(basTar, bitTar, select_user);
            ASPxGridView1.DataBind();
        }

        protected void ASPxMenu1_ItemClick(object source, DevExpress.Web.ASPxMenu.MenuItemEventArgs e)
        {
            MESAJKAPAT();
            if (e.Item.Name == "SORGU")
            {
                YUKLE();

            }
            if (e.Item.Name == "EXCEL")
            {
                ASPxGridViewExporterKullanici.WriteXlsToResponse();

            }
            
        }

       

        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
    }
}