﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_YeniKullanici.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_YeniKullanici" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style type="text/css">
    .style5 {
        width: 1px;
    }

    .style7 {
        width: 90px;
    }

    .style8 {
        width: 50%;
    }

    .style10 {
        width: 93px;
    }

    .style13 {
    }

    .style15 {
        width: 33%;
    }

    .auto-style1 {
        width: 90px;
        height: 31px;
    }

    .auto-style2 {
        width: 1px;
        height: 31px;
    }

    .auto-style3 {
        height: 31px;
    }
</style>
<script type="text/javascript">
    function popup() {
        departman.Show();
    }
    function yetki() {
        personelyetki.Show();
    }
    function grup() {
        popgrup.Show();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <h3>
            <i class="fa fa-users"></i>Kullanıcı İşlemi
        </h3>
        <asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
            <div class="alert alert-success alert-dismissable" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
                İşleminiz başarı ile gerçekleşti.
            </div>
        </asp:Panel>
        <asp:Panel ID="error" runat="server" Visible="false" Width="100%">
            <div class="alert alert-danger" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
            </div>
        </asp:Panel>
        <asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
            <div class="alert alert-warning" style="width: 100%">
                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
            </div>
        </asp:Panel>
        <dx:ASPxPopupControl ID="ASPxPopupControl1" runat="server"
            ClientInstanceName="departman" CloseAction="CloseButton"
            HeaderText="Departmanlar" Modal="True" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Theme="MetropolisBlue" Width="500px">
            <ContentStyle>
                <Paddings Padding="2px" />
            </ContentStyle>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
                        EnableTheming="True" KeyFieldName="ID" Theme="Moderno" Width="100%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Departman Adı" FieldName="Name"
                                ShowInCustomizationForm="True" VisibleIndex="2" Width="95%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Seç" ShowInCustomizationForm="True"
                                VisibleIndex="5" Width="5%">
                                <DataItemTemplate>
                                    <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/images/Apply_16x16.png"
                                        CommandArgument='<%# Eval("ID")+";"+Eval("Name")%>' OnCommand="ImageButton3_Command" />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="True" />
                        <SettingsPager>
                            <Summary Text="Sayfa {0} / {1} ({2} veri)" />
                        </SettingsPager>
                        <Settings VerticalScrollableHeight="300" />
                        <SettingsText EmptyDataRow="Veri yok..." />
                        <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
                    </dx:ASPxGridView>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server"
            ClientInstanceName="personelyetki" CloseAction="CloseButton"
            HeaderText="Yetkiler" Modal="True" PopupHorizontalAlign="WindowCenter"
            PopupVerticalAlign="WindowCenter" Theme="MetropolisBlue" Width="500px">
            <ContentStyle>
                <Paddings Padding="2px" />
            </ContentStyle>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False"
                        EnableTheming="True" KeyFieldName="YT_ID" Theme="Moderno" Width="100%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Yetki Adı" FieldName="YT_YETKIADI"
                                ShowInCustomizationForm="True" VisibleIndex="2" Width="95%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Seç" ShowInCustomizationForm="True"
                                VisibleIndex="5" Width="5%">
                                <DataItemTemplate>

                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Apply_16x16.png"
                                        CommandArgument='<%# Eval("YT_YETKIID")+";"+Eval("YT_YETKIADI")%>' OnCommand="ImageButton1_Command" />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="True" />
                        <SettingsPager>
                            <Summary Text="Sayfa {0} / {1} ({2} veri)" />
                        </SettingsPager>
                        <Settings VerticalScrollableHeight="300" />
                        <SettingsText EmptyDataRow="Veri yok..." />
                        <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
                    </dx:ASPxGridView>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <dx:ASPxPopupControl ID="ASPxPopupControl3" runat="server" ClientInstanceName="popgrup" CloseAction="CloseButton" HeaderText="Gruplar" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Theme="MetropolisBlue" Width="500px">
            <ContentStyle>
                <Paddings Padding="2px" />
            </ContentStyle>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl3" runat="server">
                    <dx:ASPxGridView ID="ASPxGridView3" runat="server" AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="GRP_ID" Theme="Moderno" Width="100%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Grup Adı" FieldName="GRP_ADI" ShowInCustomizationForm="True" VisibleIndex="2" Width="95%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Seç" ShowInCustomizationForm="True" VisibleIndex="5" Width="5%">
                                <DataItemTemplate>
                                    <asp:ImageButton ID="ImageButton2" runat="server" CommandArgument='<%# Eval("GRP_KODU")+";"+Eval("GRP_ADI")%>' ImageUrl="~/images/Apply_16x16.png" OnCommand="ImageButton2_Command" />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="True" />
                        <SettingsPager>
                            <Summary Text="Sayfa {0} / {1} ({2} veri)" />
                        </SettingsPager>
                        <Settings VerticalScrollableHeight="300" />
                        <SettingsText EmptyDataRow="Veri yok..." />
                        <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
                    </dx:ASPxGridView>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
        <table style="width: 100%;">
            <tr>
                <td class="style8" valign="top">
                    <table style="width: 100%;">
                        <tr>
                            <td class="style7">
                                <asp:Label ID="Label4" runat="server" Text="Kullanıcı Adı"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txt_kullaniciadi" runat="server" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <asp:Label ID="Label32" runat="server" Text="İsim"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txt_isim" runat="server" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                               
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <asp:Label ID="Label27" runat="server" Text="Soyisim"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td>
                                <dx:ASPxTextBox ID="txt_soyisim" runat="server" Theme="Moderno" Width="100%" NullText="Zorunlu alan...">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>

                        <tr>
                            <td class="auto-style1">
                                <asp:Label ID="Label8" runat="server" Text="Departman"></asp:Label>
                            </td>
                            <td class="auto-style2">:</td>
                            <td class="auto-style3">
                              
                                <dx:ASPxButtonEdit ID="txt_departman" runat="server" MaxLength="25" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                                    <ClientSideEvents ButtonClick="function(s, e) {	popup();}" />
                                    <Buttons>
                                        <dx:EditButton>
                                        </dx:EditButton>
                                    </Buttons>
                                </dx:ASPxButtonEdit>

                            </td>
                        </tr>
                        <tr>
                            <td class="style7">&nbsp;</td>
                            <td class="style5">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
                <td class="style8" valign="top" align="right">
                    <table style="width: 100%;">
                        <tr>
                            <td class="style10" align="left">
                                <asp:Label ID="Label15" runat="server" Text="Yetki"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxButtonEdit ID="txt_yetki" runat="server" NullText="Zorunlu alan..." ReadOnly="True" Theme="Moderno" Width="100%">
                                    <ClientSideEvents ButtonClick="function(s, e) {	yetki();}" />
                                    <Buttons>
                                        <dx:EditButton>
                                        </dx:EditButton>
                                    </Buttons>
                                </dx:ASPxButtonEdit>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <asp:Label ID="Label31" runat="server" Text="Grup"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxButtonEdit ID="txt_grup" runat="server" NullText="Zorunlu alan..." ReadOnly="True" Theme="Moderno" Width="100%">
                                    <ClientSideEvents ButtonClick="function(s, e) {	grup();}" />
                                    <Buttons>
                                        <dx:EditButton>
                                        </dx:EditButton>
                                    </Buttons>
                                </dx:ASPxButtonEdit>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="style10">
                                <asp:Label ID="Label29" runat="server" Text="Şifre"></asp:Label>
                                *</td>
                            <td class="style5">:</td>
                            <td align="left" colspan="2">
                                <dx:ASPxTextBox ID="txt_sifre" runat="server" MaxLength="25" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style10" align="left">
                                <asp:Label ID="Label24" runat="server" Text="Aktif/Pasif"></asp:Label>
                            </td>
                            <td class="style5">:</td>
                            <td align="left">
                                <dx:ASPxCheckBox ID="aktifpasif" runat="server" Theme="MetropolisBlue">
                                </dx:ASPxCheckBox>
                            </td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style10" align="left">&nbsp;</td>
                            <td class="style5">&nbsp;</td>
                            <td align="left" colspan="2">&nbsp;</td>
                        </tr>
                    </table>
                    <table style="width: 100%;">
                        <tr>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton4" runat="server" OnClick="ASPxButton4_Click"
                                    Text="Yeni" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" OnClick="ASPxButton1_Click"
                                    Text="Kaydet" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>

                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton3" runat="server" OnClick="ASPxButton3_Click"
                                    Text="Vazgeç" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>







