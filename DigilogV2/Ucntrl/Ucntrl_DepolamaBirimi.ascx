﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_DepolamaBirimi.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_DepolamaBirimi" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxMenu" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
  <link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<h3>
                                <i class="fa fa-phone-square"></i> Depolama Yönetimi
                            </h3>
 <%--<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-success alert-dismissable" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
                                İşleminiz başarı ile gerçekleşti.
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="error" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-danger" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-warning" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
                            </div>
                        </asp:Panel>--%>

<dx:ASPxDataView ID="dtlstStorageMedia" runat="server" EnableTheming="True"
    ItemSpacing="1px" PagerPanelSpacing="1px" Theme="Office2010Blue" 
    Width="100%" OnDataBinding="dtlstStorageMedia_DataBinding" 
    OnDataBound="dtlstStorageMedia_DataBound">
    <SettingsTableLayout ColumnCount="3" />
    <PagerSettings ShowNumericButtons="False"></PagerSettings>
    <ItemStyle Width="33%" />
    <ItemTemplate>
        <table style="width:100%">
            <tr>
                <td class="style1">
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' Font-Bold="True" Font-Size="15pt" ForeColor="#FF3300"></asp:Label>
                    <img src="images/fixed.gif">
                </td>
                <td class="style2">:</td>
                <td>
                
                    <asp:Image runat="server" ID="imgBlueLine" ImageUrl="~/images/BlueLine.gif" Width='<%#  System.Web.UI.WebControls.Unit.Parse(Eval("Wdh").ToString())  %>'
                        Height="10" />

                    <asp:Image runat="server" ID="imgGreenLine" ImageUrl="~/images/GreenLine.gif"
                        Width='<%#  System.Web.UI.WebControls.Unit.Parse(Eval("Wdth").ToString())  %>' Height="10" />
                </td>
            </tr>
            <tr>
                <td class="style1" colspan="3">
                    <hr style="width:100%" />

                </td>
            </tr>
            <tr>
                <td style="width:40%">
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="13pt">Boş Alan</asp:Label>
                </td>
                <td class="style2">:</td>
                <td style="width:60%">
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Frspc") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">&nbsp;</td>
                <td class="style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="style3">
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="13pt">Toplam Boyut</asp:Label>
                </td>
                <td class="style4">:</td>
                <td class="style5">
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Sz") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style3">&nbsp;</td>
                <td class="style4">&nbsp;</td>
                <td class="style5">&nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="13pt">Kullanım</asp:Label>
                </td>
                <td class="style2">:</td>
                <td>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Usg") %>'></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">&nbsp;</td>
                <td class="style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Font-Size="13pt">Boş</asp:Label>
                </td>
                <td class="style2">:</td>
                <td>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Free") %>'></asp:Label>
                </td>
            </tr>
        </table>
    </ItemTemplate>
    <ContentStyle>
        <Paddings Padding="5px" />
    </ContentStyle>
    <ItemStyle>
        <Paddings Padding="5px" />
    </ItemStyle>
</dx:ASPxDataView>