﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_YeniDepartman.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_YeniDepartman" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
  <link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<style type="text/css">
    .style5
    {
        width: 1px;
    }
    .style7
    {
        width: 90px;
    }
    .style8
    {
        width: 50%;
    }
    .style10
    {
        width: 93px;
    }
    .style13
    {
    }
     .style15
    {
        width: 33%;
    }
    .auto-style1 {
        width: 90px;
        height: 31px;
    }
    .auto-style2 {
        width: 1px;
        height: 31px;
    }
    .auto-style3 {
        height: 31px;
    }
</style>
<script type="text/javascript">
   
    function sube() {
        subeler.Show();
    }
    
    </script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
         <h3>
                                <i class="fa fa-users"></i> Yeni Departman
                            </h3>
          <asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-success alert-dismissable" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
                                İşleminiz başarı ile gerçekleşti.
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="error" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-danger" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
                            <div class="alert alert-warning" style="width: 100%">
                                <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
                            </div>
                        </asp:Panel>
       
        <dx:ASPxPopupControl ID="ASPxPopupControl2" runat="server" 
            ClientInstanceName="subeler" CloseAction="CloseButton" 
            HeaderText="Şubeler" Modal="True" PopupHorizontalAlign="WindowCenter" 
            PopupVerticalAlign="WindowCenter" Theme="MetropolisBlue" Width="500px">
            <ContentStyle>
                <Paddings Padding="2px" />
            </ContentStyle>
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" 
                        EnableTheming="True" KeyFieldName="BranchCode" Theme="Moderno" Width="100%">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Şube Adı" FieldName="Name" 
                                ShowInCustomizationForm="True" VisibleIndex="2" Width="95%">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Seç" ShowInCustomizationForm="True" 
                                VisibleIndex="5" Width="5%">
                                <DataItemTemplate>
                                   
                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/Apply_16x16.png"  
                                        CommandArgument='<%# Eval("BranchCode")+";"+Eval("Name")%>' OnCommand="ImageButton1_Command" />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="True" />
                        <SettingsPager>
                            <Summary Text="Sayfa {0} / {1} ({2} veri)" />
                        </SettingsPager>
                        <Settings VerticalScrollableHeight="300" />
                        <SettingsText EmptyDataRow="Veri yok..." />
                        <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
                    </dx:ASPxGridView>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
         
        <table style="width:100%;">
            <tr>
                <td class="style8" valign="top">
                    <table style="width: 100%;">
                          <tr>
                            <td class="style10" align="left">
                                <asp:Label ID="Label15" runat="server" Text="Şube"></asp:Label>
                                *</td>
                            <td class="style5">
                                :</td>
                            <td align="left" colspan="2">
                                <dx:ASPxButtonEdit ID="txt_sube" runat="server" NullText="Zorunlu alan..." ReadOnly="True" Theme="Moderno" Width="100%">
                                    <ClientSideEvents ButtonClick="function(s, e) {
	sube();
}" />
                                    <Buttons>
                                        <dx:EditButton>
                                        </dx:EditButton>
                                    </Buttons>
                                </dx:ASPxButtonEdit>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                <asp:Label ID="Label4" runat="server" Text="Departman Adı"></asp:Label>
                                *</td>
                            <td class="style5">
                                :</td>
                            <td>
                                <dx:ASPxButtonEdit ID="txt_departmanadi" runat="server" MaxLength="25" NullText="Zorunlu alan..." Theme="Moderno" Width="100%">
                                    <ClientSideEvents ButtonClick="function(s, e) {
	popup();
}" />
                                    <Buttons>
                                        <dx:EditButton Visible="False">
                                        </dx:EditButton>
                                    </Buttons>
                                </dx:ASPxButtonEdit>
                            </td>
                        </tr>
                     <tr>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton4" runat="server" onclick="ASPxButton4_Click" 
                                    Text="Yeni" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" onclick="ASPxButton1_Click" 
                                    Text="Kaydet" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                            
                            <td align="center" class="style15">
                                <dx:ASPxButton ID="ASPxButton3" runat="server" onclick="ASPxButton3_Click" 
                                    Text="Vazgeç" Theme="Moderno" Width="100%" Enabled="False">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="style7">
                                &nbsp;</td>
                            <td class="style5">&nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
               
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>







