﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HelperDataLib;
using System.Data;

namespace DigilogV2.Ucntrl
{
    public partial class Ucntrl_YeniSube : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (!Page.IsPostBack)
                {
                    if (Request.QueryString["id"] != null && Request.QueryString["new"] == "False")
                    {
                        //Session["kullanicisession"] = HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString());
                        DETAY(HelperDataLib.Encrypt.UrlDecrypt(Request.QueryString["id"].ToString()));
                    }
                    if (Request.QueryString["new"] == "True")
                    {
                        Session["kullanicisession"] = null;
                    }
                    YETKIKONTROL();
                    HttpContext.Current.Cache["CacheDataTableYetki"] = HelperDataLib.Select.S_YETKI_YUKLE();
                    HttpContext.Current.Cache["CacheDataTableGrup"] = HelperDataLib.Select.S_GRUP_LISTESI();
                }
            }
           
        }
        void DETAY(string id)
        {
            DataRow sube = HelperDataLib.Select.S_SUBE_YUKLE().Select("BranchCode = " + id + "").FirstOrDefault();
            //,[]
            //,[]
            //,[]
            //,[]
            //,[IsConnected]
            //,[ConnectedDate]
            //,[]
            //,[]
            //,[]
            //,[]
            //,[City]

            //lblSubeDurumu.Text = sube[""].ToString();
            txtSubeIsmi.Text = sube["Name"].ToString();
            txtSubeKodu.Text = sube["BranchCode"].ToString();
            txtSubeKanal.Text = sube["Channel"].ToString();
            //lblDisIpAdresi.Text = sube[""].ToString();
            txtIpAddress.Text = sube["IpAddress"].ToString();
            txtSqlServerName.Text = sube["SSName"].ToString();
            //txtSunucuSqlExpressmi.Text = sube["SqlExpress"].ToString();
            txtDatabase.Text = sube["DataBase"].ToString();
            //lblYedektenSorgulama.Text = sube[""].ToString();
            //lblYedekIpAdresi.Text = sube[""].ToString();
            //lblYedekSqlServerName.Text = sube[""].ToString();
            //lblYedekSqlExpress.Text = sube[""].ToString();
            //lblYedekDatabase.Text = sube[""].ToString();
            //lblYedekTablo.Text = sube[""].ToString();
            //lblYedeklemeYolu.Text = sube[""].ToString();
            //lblZamanAsimi.Text = sube[""].ToString();
            //lblVirtualPath.Text = sube[""].ToString();
            //lblTopluIndirmeYolu.Text = sube[""].ToString();
            //lblTopluIndirmeSanalDizin.Text = sube[""].ToString();
            //lblSmtpAdres.Text = sube[""].ToString();
            //lblSmtpPort.Text = sube[""].ToString();
            //lblMailKullaniciAdi.Text = sube[""].ToString();
            //lblMailSifre.Text = sube[""].ToString();
            //lblGonderenMailAdres.Text = LangLib.Xml.BilgiAl("TR102", value, text);
            //lblGidenMAilAdres.Text = LangLib.Xml.BilgiAl("TR103", value, text);
            //lblMailOnceligi.Text = LangLib.Xml.BilgiAl("TR104", value, text);
            //lblDigerSecenekler.Text = LangLib.Xml.BilgiAl("TR105", value, text);
            checkAktif.Text = sube["IsActive"].ToString();
            checkSqlExpress.Text = sube["SqlExpress"].ToString();
            //checkYedeklemeTablosu.Text = sube["Name"].ToString();
            //checkYedekSqlExpress.Text = sube["Name"].ToString();
            //checkDısIP.Text = sube["Name"].ToString();

        }
        void MESAJKAPAT()
        {
            succes.Visible = false;
            error.Visible = false;
            Warning.Visible = false;
        }
        void YETKIKONTROL()
        {
            bool OKU = false; bool YAZ = false; bool SIL = false; string USERS = ""; bool EKSTRA = false;
            HelperDataLib.Select.S_YETKI_GETVALUES("1", ref OKU, ref YAZ, ref SIL, ref EKSTRA, ref USERS);
            if (YAZ == true)
            {

                ASPxButton1.Enabled = true;
                ASPxButton3.Enabled = true;
                ASPxButton4.Enabled = true;
            }
            if (OKU == false)
            {
                Response.Redirect("~/Default.aspx");
            }

        }

        void TEMIZLE()
        {
            txtSubeIsmi.Text = "";
            txtSubeKodu.Text = "";
            txtSubeKanal.Text = "";
            txtIpAddress.Text = "";
            txtSqlServerName.Text = "";
            txtDatabase.Text = "";
            checkAktif.Checked = false;
            checkSqlExpress.Checked = false;
            Session["yetkiid"] = null;
            Session["grupid"] = null;
            Session["kullanicisession"] = null;

        }
        void KAYIT(ref bool durum)
        {
            MESAJKAPAT();
            //kayıt
            if (Session["kullanicisession"] == null)
            {
                if (txtSubeIsmi.Text != "" && txtSubeKodu.Text != "" && txtSubeKanal.Text != "" && txtIpAddress.Text != "" && txtSqlServerName.Text != "" && txtDatabase.Text != "")
                {
                    HelperDataLib.Insert.I_SUBE(txtSubeKodu.Text, txtSubeIsmi.Text, checkAktif.Checked, txtSubeKanal.Text, false, DateTime.Now, txtIpAddress.Text, checkSqlExpress.Checked, txtDatabase.Text, txtSqlServerName.Text, "34", ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            else
            {
                if (txtSubeIsmi.Text != "" && txtSubeKodu.Text != "" && txtSubeKanal.Text != "" && txtIpAddress.Text != "" && txtSqlServerName.Text != "" && txtDatabase.Text != "")
                {
                    HelperDataLib.Update.U_SUBE(txtSubeKodu.Text, txtSubeIsmi.Text, checkAktif.Checked, txtSubeKanal.Text, false, DateTime.Now, txtIpAddress.Text, checkSqlExpress.Checked, txtDatabase.Text, txtSqlServerName.Text, "34", ref durum);
                    if (durum == false)
                    {

                        error.Visible = true;


                    }
                }
                else
                {
                    Warning.Visible = true;
                }
            }
            Response.Redirect("~/SubeYonetimi.aspx");
        }
        protected void checkDısIP_CheckedChanged(object sender, EventArgs e)
        {
            //txtDisIp.Enabled = checkDısIP.Checked;
        }

        protected void ASPxButton4_Click(object sender, EventArgs e)
        {
            //yeni
            TEMIZLE();
        }

        protected void ASPxButton1_Click(object sender, EventArgs e)
        {
            //kaydet
            bool durum = false;
            KAYIT(ref durum);
            if (durum == true)
            {
                TEMIZLE();
                succes.Visible = true;
            }
        }
        protected void ASPxButton3_Click(object sender, EventArgs e)
        {
            //vazgeç
            TEMIZLE();
            Response.Redirect("~/SubeYonetimi.aspx");
        }
       

    }
}