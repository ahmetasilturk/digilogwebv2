﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ucntrl_KayitSorgulama.ascx.cs" Inherits="DigilogV2.Ucntrl.Ucntrl_KayitSorgulama" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v14.1, Version=14.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<link rel="Shortcut Icon" href="~/Logo/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<h3>
    <i class="fa fa-phone-square"></i>Kayıt Sorgula
</h3>
<asp:Panel ID="succes" runat="server" Visible="false" Width="100%">
    <div class="alert alert-success alert-dismissable" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Başarılı!</strong>
        İşleminiz başarı ile gerçekleşti.
    </div>
</asp:Panel>
<asp:Panel ID="error" runat="server" Visible="false" Width="100%">
    <div class="alert alert-danger" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Hata!</strong> İşleminiz
                                hata aldı!
    </div>
</asp:Panel>
<asp:Panel ID="Warning" runat="server" Visible="false" Width="100%">
    <div class="alert alert-warning" style="width: 100%">
        <a class="close" data-dismiss="alert" href="#">×</a> <strong>Uyarı!</strong> (*)
                                alanları yazınız!
    </div>
</asp:Panel>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
         <div align="right">
            <dx:ASPxLabel ID="lblKayitSayisi" runat="server" Text="Kayıt Sayısı:">
            </dx:ASPxLabel>
            <dx:ASPxComboBox ID="comboKayitSayisi" runat="server" AutoPostBack="True"
                EnableTheming="True"
                OnSelectedIndexChanged="comboKayitSayisi_SelectedIndexChanged"
                Theme="Moderno" ValueType="System.Int32">
            </dx:ASPxComboBox>
        </div>
        <hr />

        <table style="width: 100%;">
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Başlama Tarih-Saat"></asp:Label>
                </td>
                <td class="style2">:</td>
                <td class="style3">
                    <dx:ASPxDateEdit ID="txtStartDate" runat="server" Theme="Moderno">
                    </dx:ASPxDateEdit>
                </td>
                <td class="auto-style2">
                    <dx:ASPxTimeEdit ID="txtStartTime" runat="server" Theme="Moderno"
                        Width="100px">
                    </dx:ASPxTimeEdit>
                </td>
                <td class="auto-style1">
                    <asp:Label ID="Label3" runat="server" Text="Bitiş Tarih-Saat"></asp:Label>
                </td>
                <td class="style6">:</td>
                <td class="style3">
                    <dx:ASPxDateEdit ID="txtEndDate" runat="server" Theme="Moderno">
                    </dx:ASPxDateEdit>
                </td>
                <td class="style5">
                    <dx:ASPxTimeEdit ID="txtEndTime" runat="server" Theme="Moderno"
                        Width="100px">
                    </dx:ASPxTimeEdit>
                </td>
                <td class="style7">&nbsp;</td>
                <td>
                    <asp:Label ID="lblResult" runat="server" Visible="true"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label4" runat="server" Text="Aranan No"></asp:Label>
                </td>
                <td class="style2">:</td>
                <td class="style3">
                    <dx:ASPxTextBox ID="txtCalled" runat="server" Width="170px"
                        Theme="Moderno">
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style1">
                    <asp:Label ID="Label5" runat="server" Text="Arayan No"></asp:Label>
                </td>
                <td class="style6">:</td>
                <td class="style3">
                    <dx:ASPxTextBox ID="txtCaller" runat="server" Width="170px"
                        Theme="Moderno">
                    </dx:ASPxTextBox>
                </td>
                <td class="style5">&nbsp;</td>
                <td class="style7">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label6" runat="server" Text="Arama Tipi"></asp:Label>
                </td>
                <td class="style2">:</td>
                <td class="style3">
                   <%-- <dx:ASPxComboBox ID="ddlCallType" runat="server" Theme="Office2010Blue">
                    </dx:ASPxComboBox>--%>
                </td>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style1">
                    <asp:Label ID="Label12" runat="server" Text="Dahili"></asp:Label>
                </td>
                <td class="style6">:</td>
                <td class="style3">
                    <%--<dx:ASPxComboBox ID="dahili" runat="server" Theme="Office2010Blue">
                    </dx:ASPxComboBox>--%>
                </td>
                <td class="style5">&nbsp;</td>
                <td class="style7">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label8" runat="server" Text="Süre"></asp:Label>
                </td>
                <td class="style2">:</td>
                <td class="style3">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <dx:ASPxTextBox ID="txtDurationStart" runat="server" Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                            <td class="style7">-</td>
                            <td>
                                <dx:ASPxTextBox ID="txtDurationEnd" runat="server" Theme="Moderno" Width="100%">
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
                <td class="style6">&nbsp;</td>
                <td class="style3">&nbsp;</td>
                <td class="style5">&nbsp;</td>
                <td class="style7">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Label ID="Label11" runat="server" Text="Id:"></asp:Label>
                </td>
                <td class="style2">:</td>
                <td class="style3">
                    <dx:ASPxTextBox ID="txtId" runat="server" Theme="Moderno" Width="170px">
                    </dx:ASPxTextBox>
                </td>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
                <td class="style6">&nbsp;</td>
                <td class="style3">&nbsp;</td>
                <td class="style5">&nbsp;</td>
                <td class="style7">&nbsp;</td>
                <td colspan="2">&nbsp;</td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<div style="width: 100%">
    <dx:ASPxMenu ID="ASPxMenu1" runat="server" AutoSeparators="RootOnly"
        EnableTheming="True" HorizontalAlign="Left" ItemAutoWidth="False"
        OnItemClick="ASPxMenu1_ItemClick" Theme="Moderno" Width="100%">
        <Items>
            <dx:MenuItem Text="Sorgula" Name="ISLEM">
                <Image Url="~/images/sorgu.png">
                </Image>
            </dx:MenuItem>
            <dx:MenuItem Text="Excel" Name="EXCEL">
                <Image Url="~/images/excel.png">
                </Image>
            </dx:MenuItem>
            <dx:MenuItem Text="Grafik" Name="GRAFIK" Visible="False">
                <Image Url="~/images/grafik.png">
                </Image>
            </dx:MenuItem>
        </Items>
    </dx:ASPxMenu>
</div>

<asp:UpdatePanel ID="UpdatePanel2" runat="server">
    <ContentTemplate>
        <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
            EnableTheming="True" Theme="MetropolisBlue" Width="100%" KeyFieldName="Ref">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Id" VisibleIndex="0" Width="10%" FieldName="Ref">
                    <Settings FilterMode="DisplayText" />
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Aranan No" VisibleIndex="4" Width="10%" FieldName="Called">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Width="10%"
                    Caption="Se&#231;" VisibleIndex="8">
                    <DataItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/dinle.png" CommandArgument='<%# Eval("FilePath")  %>' CommandName="cmdPlay"
                            ToolTip="Dinle" OnClick="ImageButton1_Click" />

                        &nbsp;<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/indir.png" CommandArgument='<%# Eval("FilePath")  %>' CommandName="cmdDownload"
                            ToolTip="İndir" OnClick="ImageButton2_Click" />
                        &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/images/mail.png" CommandArgument='<%# Eval("FilePath") %>' CommandName="cmdEmail"
                            ToolTip="Mail" OnClick="ImageButton3_Click" />
                        &nbsp;<asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/images/sil.png" CommandArgument='<%# Eval("Ref") %>' OnClientClick='<%# "return CheckDel(" + Eval("Ref") + ");" %>' CommandName="cmdDelete"
                            ToolTip="Sil" OnClick="ImageButton4_Click" Style="width: 16px" />

                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Arayan No" VisibleIndex="3" Width="10%" FieldName="Caller">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Arama Tipi" VisibleIndex="2" Width="10%" FieldName="CallType">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Başlama Tarihi" VisibleIndex="5"
                    Width="10%" FieldName="StartDateTime">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bitiş Tarihi" VisibleIndex="6" Width="10%" FieldName="EndDateTime">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Konuşma Süre" VisibleIndex="7" Width="5%" FieldName="Duration">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Dahili" FieldName="extno" VisibleIndex="1" Width="10%">
                </dx:GridViewDataTextColumn>
            </Columns>
            <SettingsBehavior AllowFocusedRow="True" />
            <SettingsPager>
                <Summary Text="Sayfa {0} / {1} ({2} veri)" />
            </SettingsPager>
            <Settings ShowFilterRow="True" />
            <SettingsText EmptyDataRow="Veri yok..." />
            <SettingsLoadingPanel Text="Yükleniyor&amp;hellip;" />
        </dx:ASPxGridView>
        <dx:ASPxGridView ID="dgRecordReports0" runat="server"
            AutoGenerateColumns="False" EnableTheming="True" Theme="Office2010Blue"
            Width="100%" Visible="False">
            <Columns>
                <dx:GridViewDataTextColumn Caption="Gelen Ç. Sayısı" FieldName="ITCount" VisibleIndex="0" Width="10%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Gelen Ç. Top Sür" FieldName="ITDur" VisibleIndex="1" Width="10%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Giden Ç. Sayısı" FieldName="OTCount" VisibleIndex="4" Width="10%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Giden Ç. Top Sür" FieldName="OTDur"
                    VisibleIndex="5" Width="10%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bilinmeyen Ç. Sayısı" FieldName="UTCount"
                    VisibleIndex="6" Width="10%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Bilinmeyen Ç. Top Sür" FieldName="UTDur"
                    VisibleIndex="7" Width="5%">
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Toplam Çağrı Süresi" FieldName="TDur"
                    VisibleIndex="8" Width="10%">
                </dx:GridViewDataTextColumn>

            </Columns>
            <SettingsPager>
                <Summary Text="Sayfa {0} /{1} ({2} veri)" />
            </SettingsPager>
            <Settings ShowFilterRow="True" />
            <SettingsText EmptyDataRow="Veri Yok..." />
        </dx:ASPxGridView>
    </ContentTemplate>
</asp:UpdatePanel>

<dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server">
</dx:ASPxGridViewExporter>
