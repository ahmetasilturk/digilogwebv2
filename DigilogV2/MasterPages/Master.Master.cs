﻿using DevExpress.Web.ASPxPopupControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigilogV2.MasterPages
{
       public partial class Master : System.Web.UI.MasterPage
       {
              protected void Page_Load(object sender, EventArgs e)
              {
                     if (!Page.IsPostBack)
                     {
                            if (HttpContext.Current.Session["SS_ID"] != null && HttpContext.Current.Session["SS_YETKIID"] != null && HttpContext.Current.Session["SS_USERNAME"] != null)
                            {
                                   Label8.Text = "Merhaba; "+HttpContext.Current.Session["SS_ISIMSOYISIM"].ToString();
                            }
                            else
                            {
                                   Response.Redirect("~/Login.aspx");
                            }
                     }
              }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {

            if (txtMevcutSifre.Text != "" && txtYeniSifre.Text != "")
            {
                bool durum = false;
                HelperDataLib.Select.S_GETSIFRE(HttpContext.Current.Session["SS_USERNAME"].ToString(), txtMevcutSifre.Text, ref durum);
                if (durum)
                {

                    HelperDataLib.Update.U_SIFRE(HttpContext.Current.Session["SS_USERNAME"].ToString(), txtYeniSifre.Text, ref durum);
                    Response.Redirect("~/Login.aspx");
                }
            }

        }

        protected void btnTemizle_Click(object sender, EventArgs e)
        {
            txtYeniSifre.Text = "";
            txtMevcutSifre.Text = "";
        }
    }
}